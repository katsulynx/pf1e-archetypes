_id: b8NKIvB8r98Uioid
_key: '!items!b8NKIvB8r98Uioid'
folder: DRL9ZeqhGEgMjWzJ
img: systems/pf1/icons/skills/blood_11.jpg
name: Brawler (Winding Path Renegade)
system:
  associations:
    classes:
      - Brawler
  description:
    value: >-
      <p>In the sacred text Unbinding the Fetters, Irori teaches that the path
      to transcendence can be straight or twisted. Those who follow the straight
      path adhere to the traditions and techniques perfected by others who have
      traveled before them, while those who seek the twisted path abandon
      tradition and follow their own unique routes to self-perfection. Both
      paths can lead to the same place, but those who follow the twisted path
      must take care to not lose their way.</p><p>Winding path renegades
      epitomize those who have chosen the twisted path. These brawlers initially
      studied at the Houses of Perfection in Jalmeray, but while they grasped
      the mechanical aspects of the forms, they lacked the mental discipline
      required by the masters of the school. Thus, they have withdrawn from
      their monasteries—some peacefully, some not—to find their own
      paths.</p><p><strong>School Focus (Ex)</strong>: At 2nd level, a winding
      path renegade continues the training she left behind, gaining the benefits
      of the mystery taught at her old monastery. As the winding path renegade
      progresses in her self-guided training, she gains a deeper understanding
      of her chosen monastery’s mystery, which grants her new
      powers.</p><p><strong>Mystery of Unblinking Flame (Su)</strong>: At 2nd
      level, the winding path renegade increases her speed by 10 feet (this is
      treated as an enhancement bonus). If she uses her monk moves class ability
      to gain fast movement, the abilities stack with each other. At 8th level,
      if the winding path renegade begins her turn with the grappled condition,
      she can deal 2d6 points of fire damage to all other creatures in the
      grapple as a free action. At 14th level, the winding path renegade becomes
      as difficult to contain as a flame. Once per day as a full-round action,
      she can transform into liquid flame, instantly escaping all bonds as if
      under the effect of
      @UUID[Compendium.pf1.spells.Item.lvzq2mwkqmozolpl]{freedom of movement}
      and moving up to her speed. When moving as part of this ability, she can
      move through creatures’ squares freely and doesn’t provoke attacks of
      opportunity. She deals 10d6 points of fire damage to any creature whose
      square she enters during this movement. A successful Reflex saving throw
      (DC = 10 + half her brawler level + her Dexterity bonus) halves this
      damage.</p><p><strong>Mystery of Unfolding Wind (Su)</strong>: At 2nd
      level, the winding path renegade adds 10 to the range increments of ranged
      weapons she wields and gains
      @UUID[Compendium.pf1.feats.Item.ACEQfonKVqh0fwkK]{Deflect Arrows} as a
      bonus feat. At 8th level, she can harness the power of the wind to make
      enormous leaps. As a move action, she can leap without attempting an
      Acrobatics check, jumping any distance up to her speed (upward movement
      counts as double, as when flying). She can leap in this way a number of
      times per day equal to her class level. At 14th level, when using the
      @UUID[Compendium.pf1.feats.Item.ACEQfonKVqh0fwkK]{Deflect Arrows} feat,
      the winding path renegade can send one projectile back at her attacker,
      using the same attack roll and damage roll the attacker rolled against
      her. Even if the deflected attack had more than one projectile (as with
      @UUID[Compendium.pf1.feats.Item.w23dO02t0poRrno0]{Manyshot}), the winding
      path renegade sends only one projectile back at the attacker. The others
      are deflected as normal.</p><p><strong>Mystery of Untwisting Iron
      (Su)</strong>: At 2nd level, metal weapons and armor in the winding path
      renegade’s possession are treated as masterwork unless they have the
      broken condition. At 8th level, the winding path renegade gains a bonus
      equal to half her level on Craft checks involving metal, and she gains the
      benefits of the @UUID[Compendium.pf1.feats.Item.6hmIs98sJoJzzIq0]{Master
      Craftsman} and <a
      href="https://aonprd.com/FeatDisplay.aspx?ItemName=Craft%20Arms%20and%20Armor">Craft
      Arms and Armor</a> feats when crafting items made of metal. At 14th level,
      any weapon the winding path renegade wields is treated as
      adamantine.</p><p>This ability replaces the bonus feats gained at 2nd,
      8th, and 14th levels.</p><p><strong>Monk Moves (Ex)</strong>: At 4th
      level, a winding path renegade can use her martial flexibility ability to
      gain the benefits of certain monk abilities as well as combat feats. Each
      ability counts as one combat feat for this purpose, and the winding path
      renegade is treated as a monk of a level equal to her brawler level for
      the purpose of determining the effects of these abilities. The winding
      path renegade gains access to these abilities at the levels listed on the
      Monk Moves table above. She does not gain a ki pool, and she can’t use
      powers that require ki. The winding path renegade must be wearing light or
      no armor to benefit from this ability. This ability replaces AC
      bonus.</p><table><tbody><tr><td>Monk Ability</td><td>Level
      Gained</td></tr><tr><td>Evasion</td><td>4th</td></tr><tr><td>Fast
      movement</td><td>4th</td></tr><tr><td>Slow
      fall</td><td>4th</td></tr><tr><td>High
      jump</td><td>5th</td></tr><tr><td>Improved
      evasion</td><td>9th</td></tr></tbody></table>
  sources:
    - id: PZO1138
      pages: '105'
  subType: misc
  tags:
    - Winding Path Renegade
type: feat

