_id: y4XhLW823AfYD7nT
_key: '!items!y4XhLW823AfYD7nT'
folder: Xp99iiO8M955HvSy
img: systems/pf1/icons/items/inventory/lute.jpg
name: Bard (Mute Musician)
system:
  associations:
    classes:
      - Bard
  description:
    value: >-
      <p>A mute musician forswears speech for the unnatural songs and thunderous
      silences of the depths of space. She learns to blend light and sound in
      seemingly impossible ways. Some mute musicians look to the darker aspects
      of Desna as their inspiration for these performances, but most mute
      musicians find their muse in the alien sounds of the Elder Mythos and the
      entities that orbit the blind idiot god Azathoth, who floats in the heart
      of the Dark Tapestry.</p><p><strong>Mute (Ex)</strong>: A mute musician
      has a disability or injury (possibly self-inflicted or even psychological)
      that prevents her from speaking or vocalizing. Language-dependent effects
      (including Perform [oratory]) require the bard to be able to use
      telepathy, nonverbal languages, or writing to communicate. She cannot use
      Perform (sing) or speak languages, though she can still create audible
      bardic performances by means of a musical instrument. A mute musician can
      provide verbal and somatic components for spells she casts via any musical
      instrument in which she has ranks in the appropriate Perform
      skill.</p><p><strong>Eschew Materials (Ex)</strong>: A mute musician gains
      @UUID[Compendium.pf1.feats.Item.ElqchQT5WB625HPg]{Eschew Materials} as a
      bonus feat at 1st level.</p><p>This ability replaces bardic
      knowledge.</p><p><strong>Bardic Performance</strong>: A mute musician
      gains the following bardic performances.</p><p><strong>Symphony of Silence
      (Su)</strong>: At 3rd level, the mute musician’s music muffles all other
      sounds within a 30-foot radius. All creatures in the area of effect gain a
      +2 bonus on all saving throws made against sonic attacks or
      language-dependent effects. This bonus increases to +3 at 7th level, +4 at
      11th level, +5 at 15th level, and +6 at 19th level. Symphony of silence
      relies on audible components.</p><p>This bardic performance replaces
      inspire competence.</p><p><strong>Maddening Harmonics (Su)</strong>: At
      14th level, the mute musician can create a performance so baffling and
      discordant that it usurps all thought within a 30-foot radius with chaos
      and entropy. The mute musician can select which creatures in this area are
      affected by the maddening harmonics and which are not. All targeted
      creatures within this area must succeed at a Will saving throw (DC = 10 +
      1/2 the bard’s level + the bard’s Charisma modifier) or be confused for as
      long as they can hear the performance. If the creature succeeds at its
      saving throw, it is immune to this ability for 24 hours. This performance
      relies on audible components.</p><p>This bardic performance replaces
      frightening tune.</p><p><strong>Ceaseless Performance (Su)</strong>: At
      15th level, the mute musician can continue taking the free action to
      maintain a bardic performance even while confused, cowering in fear,
      dazed, nauseated, panicked, paralyzed, petrified, silenced, staggered,
      stunned, or unconscious. Even if the mute musician is killed, she can
      continue to take the free action to maintain her performance as long as
      she has rounds remaining. Only the utter destruction of the mute
      musician’s body (such as via
      @UUID[Compendium.pf1.spells.Item.rlfd7f64wgjfvg6e]{destruction} or
      @UUID[Compendium.pf1.spells.Item.sxyiwj0z95piv96i]{disintegrate}, or by
      reducing the bard to a negative hit point total equal to 10 × her
      Constitution score) causes the performance to end.</p><p>This ability
      replaces inspire heroics.</p><p><strong>Song of the Conjunction
      (Su)</strong>: At 18th level, the mute musician can harmonize with the
      alien chorus beyond the music of the spheres, creating a portentous cosmic
      alignment. This effect duplicates a
      @UUID[Compendium.pf1.spells.Item.ursm11405zobemi3]{gate} used to travel,
      save that the destination point must be on the same plane as the bard, but
      it is not otherwise limited by distance.</p><p>This bardic performance
      replaces mass suggestion.</p><p><strong>Insights from Beyond
      (Ex)</strong>: At 2nd level, a mute musician adds two abjuration,
      conjuration (calling), conjuration (summoning), or conjuration
      (teleportation) spells from the sorcerer/wizard spell list to her list of
      bard spells known. At 6th level and every 4 bard levels thereafter, she
      can choose two more spells to add to her spells known.</p><p>This class
      feature replaces versatile performance.</p><p><strong>Dulled Horror
      (Ex)</strong>: At 2nd level, a mute musician gains a +4 bonus on saves
      against confusion, fear, insanity effects, and the supernatural abilities
      of aberrations.</p><p>This class feature replaces
      well-versed.</p><p><strong>Eldritch Caesura (Su)</strong>: At 10th level,
      a mute musician can insert unsettling silences into her otherworldly music
      by spending an additional 1 round of bardic performance per round. This
      supernatural technique impossibly blurs the line between music and light,
      transmitting audible performances and sonic bard spells through most
      barriers save lead. This allows the music and sonic spells to affect
      creatures across planar boundaries (including on the Ethereal or Shadow
      Planes), vacuums, and areas of magical silence.</p><p>This class feature
      replaces jack-of-all-trades.</p><p>Ex-Mute Musicians: A mute musician who
      regains the ability to speak or chooses to speak aloud loses all abilities
      granted by this archetype. She can regain the archetype’s abilities by
      spending 24 hours without speaking.</p>
  sources:
    - id: PZO9297
      pages: '9'
  subType: misc
  tags:
    - Mute Musician
type: feat

