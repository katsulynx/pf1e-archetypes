_id: okHBZbgGtlRbiqlQ
_key: '!items!okHBZbgGtlRbiqlQ'
folder: Fcks2ln6l9R9rQFQ
img: systems/pf1/icons/skills/shadow_17.jpg
name: Spiritualist (Involutionist)
system:
  associations:
    classes:
      - Spiritualist
  description:
    value: >-
      <p>The Rivethun teach their followers to cultivate a well of inner power
      and to open themselves to the spiritual world, but on occasion—especially
      among neophytes who join the cult later in life—these processes overlap,
      and the acolyte invests her spiritual reservoir into some aspect of the
      world around her. Rather than bond with an existing spirit as most
      Rivethun eventually do, the involutionist creates a spirit from a piece of
      her own soul. The bond is deep, and it creates a powerful minion utterly
      loyal to her creator, but it also cuts off the involutionist’s access to
      the wider spiritual world.</p><p><strong>Divine Spellcasting</strong>: An
      involutionist is a divine spellcaster. Her spells use verbal components
      instead of thought components and somatic components instead of emotional
      components. An involutionist’s spirit phantom is her conduit to the
      divine, and it serves as her divine focus; the involutionist must either
      be within 5 feet on her spirit phantom or absorb her phantom into her
      consciousness to cast spells that require a divine focus.</p><p>This
      ability alters spellcasting.</p><p><strong>Spirit Phantom</strong>: An
      involutionist’s phantom is manifested from her own psyche and invested
      into the spirit-stuff of the world around her. Rather than an emotional
      focus, the involutionist selects a spirit from those available to the
      shaman (Pathfinder RPG Advanced Class Guide 35). A spirit phantom always
      has good Fortitude and Reflex saves, and it gains a number of ranks equal
      to the phantom’s Hit Dice in two skills depending on the spirit it
      represents, as summarized on the table
      below.</p><table><tbody><tr><td>Spirit</td><td>Associated
      Skills</td></tr><tr><td>Battle</td><td>Intimidate and Sense
      Motive</td></tr><tr><td>Bones</td><td>Heal and
      Stealth</td></tr><tr><td>Flame</td><td>Acrobatics and
      Intimidate</td></tr><tr><td>Heavens</td><td>Acrobatics and
      Fly</td></tr><tr><td>Life</td><td>Diplomacy and
      Heal</td></tr><tr><td>Lore</td><td>Knowledge (arcana) and
      Linguistics</td></tr><tr><td>Nature</td><td>Knowledge (nature) and
      Survival</td></tr><tr><td>Stone</td><td>Appraise and Knowledge
      (engineering)</td></tr><tr><td>Waves</td><td>Diplomacy and
      Swim</td></tr><tr><td>Wind</td><td>Bluff and Sense
      Motive</td></tr></tbody></table><p>When fully manifested, the
      involutionist’s spirit phantom gains the special ability normally applied
      to a shaman’s spirit animal. The spirit phantom also gains a single hex
      from the list of those provided by that spirit. At 7th, 12th, and 17th
      levels, the spirit phantom gains one additional hex from this
      list.</p><p>This ability alters phantom and replaces its emotional focus
      abilities.</p><p><strong>Spirit Manifestation (Su)</strong>: At 3rd level,
      when an involutionist uses her bonded manifestation ability, she can
      instead manifest her spirit phantom’s spirit animal special ability and
      use her spirit phantom’s hexes as if they were her own. Hexes used during
      this manifestation count toward any limited number of uses per
      day.</p><p>This ability alters bonded manifestation.</p><p><strong>Spirit
      Awareness (Sp)</strong>: At 5th level, an involutionist gains great
      insight into the spiritual influence people invest into the world around
      them. She can cast detect psychic significanceOA as a spell-like ability
      at will and analyze auraOA once per day as a spell-like ability (CL = her
      character level).</p><p>This ability replaces detect
      undead.</p><p><strong>Involuate (Sp)</strong>: At 11th level, an
      involutionist gains the ability to awaken the latent spiritual energy in
      inanimate objects. She gains the ability to cast animate objects as a
      spell-like ability once per day (CL = her character level). At 15th and
      19th levels, she can use this ability one additional time per
      day.</p><p>This ability replaces the additional uses of calm spirit
      available at 11th, 15th, and 17th levels.</p>
  sources:
    - id: PZO1138
      pages: '167'
  subType: misc
  tags:
    - Involutionist
type: feat

