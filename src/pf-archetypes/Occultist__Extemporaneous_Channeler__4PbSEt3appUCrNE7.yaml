_id: 4PbSEt3appUCrNE7
_key: '!items!4PbSEt3appUCrNE7'
folder: 3i6bW82t3LIXM0wY
img: systems/pf1/icons/items/inventory/book-purple.jpg
name: Occultist (Extemporaneous Channeler)
system:
  associations:
    classes:
      - Occultist
  description:
    value: >-
      <p>Extemporaneous channelers study the power of transformation and use
      items in unintended ways to awaken their potential.</p><p>Weapon
      Proficiency: An extemporaneous channeler is proficient with all simple
      weapons.</p><p>This alters the occultist’s weapon proficiencies.<br /><br
      />Improvisational Combatant (Ex): An extemporaneous channeler does not
      incur any penalties for using an improvised weapon. She is considered to
      have the @UUID[Compendium.pf1.feats.Item.bZ1YdzkT8NtRIEJ4]{Catch
      Off-Guard} and @UUID[Compendium.pf1.feats.Item.gGfXQq0IZrxdcZDx]{Throw
      Anything} feats for the purpose of meeting prerequisites.<br /><br
      />Fleeting Focus (Su): An extemporaneous channeler’s studies of the
      changing nature of objects give her more mental focus, which she learns to
      quickly invest in her implements throughout the day, but which she
      struggles to maintain. When an extemporaneous channeler refreshes her
      mental focus, she must spend 1 hour preparing her implements for
      spellcasting but does not immediately invest them with her generic focus,
      which is equal to her occultist level + twice her Intelligence modifier.
      At any point during the day, she can shift any amount of her generic focus
      to any number of her implements by concentrating for 1 round while
      touching her implements. After every 10 minutes, her invested implements
      each lose 1 point of mental focus, as if it had been expended on a focus
      power. If the extemporaneous channeler uses her withdraw focus ability to
      remove focus before a full 10-minute increment has elapsed, each implement
      still loses 1 point of mental focus as if it had been invested for that
      entire increment.</p><p>This alters mental focus.<br /><br
      />Transformative Resonance (Su): An extemporaneous channeler can expend 1
      point of generic focus as a swift action to allow her generic focus to
      resonate in her improvised weapons. For 1 minute, the extemporaneous
      channeler grants any item she uses as an improvised weapon a +1
      enhancement bonus on attack and damage rolls for every 3 points of generic
      focus she has in her body. She can give it a maximum bonus of +1 at 1st
      level; the maximum bonus increases by 1 for every 4 levels after 1st, to a
      maximum of +5 at 17th level. The extemporaneous channeler can imbue the
      item with any one weapon special ability with an equivalent enhancement
      bonus less than or equal to her maximum bonus by reducing the granted
      enhancement bonus by the appropriate amount, such as reducing a +2 bonus
      to a +1 <a
      href="https://aonprd.com/MagicWeaponsDisplay.aspx?ItemName=Flaming">flaming</a>
      enchantment. She chooses the special ability when she activates this
      power, but she can spend 1 point of generic focus as a swift action to
      change it, which also resets the duration of the power. The item must have
      an enhancement bonus of at least +1 (either on its own or from her imbuing
      it) to gain a weapon special ability. This ability stacks with any other
      effect that grants an enhancement bonus to an improvised weapon, such as
      <a
      href="https://aonprd.com/MagicWondrousDisplay.aspx?FinalName=Gloves%20of%20Improvised%20Might1">gloves
      of improvised might</a>, to a maximum of +5.</p><p><strong>Withdraw Focus
      (Su)</strong>: At 4th level, an extemporaneous channeler can, as a
      standard action, shift any amount of focus from any number of implements
      back into herself as generic focus. Doing so costs her no loss of focus
      (other than what’s lost due to her fleeting focus ability). Unlike
      expending focus normally, this shift can reduce the effect of a resonant
      power in the implement from which the mental focus was taken.</p><p>This
      alters shift focus.<br /><br />Improvised Spell (Su): At 8th level, an
      extemporaneous channeler learns to use her implements to improvise a spell
      similar to one she already knows. Once per day, an extemporaneous
      channeler can cast an occultist spell from an implement school she knows
      as if it were one of her spells known, expending a spell slot of the same
      level and an amount of generic focus equal to the spell’s level to cast
      the desired spell. A spell cast as an improvised spell always has a
      minimum casting time of 1 round, regardless of the casting time of the
      spell. At 12th level, an extemporaneous channeler can use this ability
      twice per day. At 17th level, she can use this ability three times per
      day.</p><p>This replaces magic circles, outside contact, binding circles,
      and fast circles.</p>
  sources:
    - id: PZO9493
      pages: '8'
  subType: misc
  tags:
    - Extemporaneous Channeler
type: feat

