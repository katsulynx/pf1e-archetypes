_id: bQIuElfE4234kAlF
_key: '!items!bQIuElfE4234kAlF'
folder: QrHzi231Uw5jbJge
img: systems/pf1/icons/items/weapons/greataxe.png
name: Barbarian (Giant Stalker)
system:
  associations:
    classes:
      - Barbarian
  description:
    value: >-
      <p>Of the dangerous creatures that roam grim peaks and icy steppes, giants
      are among the most feared. They are cunning and capricious, and their
      ability to craft weapons and coordinate attacks makes them a constant
      threat to the nomadic tribes in the region. In the wilds where giants
      hunt, cleverness isn’t the only thing the people need to survive—they
      require the fearlessness to confront a challenge, no matter the
      size.</p><p>Elders select the children of the tribe’s fiercest barbarians
      for the honor of becoming giant stalkers. Through intense training, these
      children learn to spot and track giants using telltale signs along with
      more subtle clues (such as giants’ unique scents). They learn the giants’
      speech and culture; they study their traditions and brutality; and most
      importantly, they foster their hatred of these oversized foes.</p><p>When
      Mammoth Lord followings travel, giant stalkers scout ahead, securing safe
      passage through giant-infested territories. They clear giant hunting
      parties and ambushes, using gruesome trophies to mark the trails.
      Sometimes giant hunters send their own raiding parties into the mountains
      to capture young giants, as most followings keep giant thralls as symbols
      of status.</p><p><strong>Harangue Giant (Ex)</strong>: While raging, a
      giant stalker can speak Giant. If the giant stalker already knows Giant,
      she gains a +2 bonus on Intimidate checks when speaking Giant while
      raging.</p><p>This ability alters rage.</p><p><strong>Smell Giants
      (Ex)</strong>: A 2nd level, a giant stalker gains the scent special
      ability, but with regard only to humanoids with the giant
      subtype.</p><p>This ability replaces uncanny dodge.</p><p><strong>Giant
      Baiter (Su)</strong>: At 3rd level, a raging giant stalker can take a move
      action to erupt into a violent or vulgar display directed at a single
      giant within 60 feet. This display is infused with secret magical
      traditions that draw upon the Mammoth Lords’ ancestral enmities toward
      giants. A targeted giant must succeed at a Will save (DC = 10 + half the
      giant stalker’s barbarian level + the giant stalker’s Constitution
      modifier) or be compelled to focus its attacks on the giant stalker; this
      ability can also affect non-giant creatures that are Large or larger,
      though such creatures gain a +4 bonus on the Will save to resist the
      effect. This compulsion ends if the giant stalker is out of sight or
      inaccessible to the giant’s attacks. While baited, a giant is distracted
      and enraged by the insults—a giant stalker gains a +1 dodge bonus to her
      AC against any giant she has successfully baited. Once baited, a giant
      remains baited for a number of rounds equal to the giant stalker’s
      Constitution modifier, or until the giant stalker’s rage ends (whichever
      comes first). If the giant stalker baits a different giant, any previously
      baited giant is no longer baited. A giant that successfully saves against
      baiting is immune to that giant stalker’s giant baiter ability for 24
      hours. This is a language-dependent mind affecting effect.</p><p>At 6th
      level, a giant stalker can bait two giants at once, and her AC bonus
      against baited giants increases to +2. At 9th level, baiting giants
      becomes a swift action, and her AC bonus against baited giants increases
      to +3. At 12th level, she can bait up to three giants at once, and her AC
      bonus against baited giants increases to +4. At 15th level, baiting giants
      becomes a free action, and her AC bonus against baited giants increases to
      +5. At 18th level, a giant stalker can bait a number of giants equal to 3
      + her Constitution modifier, and her AC bonus against baited giants
      increases to +6.</p><p>This ability replaces trap sense.</p><p>Giant
      Stalker Rage Powers: A giant stalker gains access to the following rage
      powers.</p><p><strong>Giant Stalker Defense (Ex)</strong>: When raging, a
      giant stalker does not take a penalty to Armor Class against attacks from
      giants.</p><p><strong>Topple Giant (Ex)</strong>: When raging, the giant
      stalker does not provoke attacks of opportunity when she attempts to trip
      a giant, and can attempt to trip a giant that is up to two size categories
      larger than she is.</p><p><strong>Underfoot (Ex)</strong>: When raging,
      the giant stalker can attempt to enter a giant’s space, provided she is at
      least one size category smaller than the giant. This does not provoke an
      attack of opportunity. Doing so takes a move action and requires a
      successful combat maneuver check against the giant’s CMD. If she succeeds,
      she enters a square in the giant’s space. If she makes an attack against
      the giant while sharing its space, the giant is treated as being
      flat-footed against her. At the end of her turn, she exits the giant’s
      space into any square adjacent to the giant’s space.</p>
  sources:
    - id: PZO1138
      pages: '130'
  subType: misc
  tags:
    - Giant Stalker
type: feat

