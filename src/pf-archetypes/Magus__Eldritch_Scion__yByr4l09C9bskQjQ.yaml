_id: yByr4l09C9bskQjQ
_key: '!items!yByr4l09C9bskQjQ'
folder: hCmBiEuqgcpSqf0f
img: systems/pf1/icons/spells/enchant-magenta-3.jpg
name: Magus (Eldritch Scion)
system:
  associations:
    classes:
      - Magus
  description:
    value: >-
      <p>Unlike typical magi, eldritch scions do not study tomes of magic or
      spend time learning to combine martial and magical skills. Rather,
      eldritch scions find that their spells and abilities come to them
      instinctively.</p><p><strong>Spells</strong>: An eldritch scion casts
      arcane spells drawn from the magus spell list. He can cast any spell he
      knows without preparing it ahead of time. To learn or cast a spell, an
      eldritch scion must have a Charisma score equal to at least 10 + the
      spell’s level. The DC for a saving throw against an eldritch scion’s spell
      is 10 + the spell’s level + the eldritch scion’s Charisma modifier. An
      eldritch scion can cast only a certain number of spells of each spell
      level per day. His base daily spell allotment is the same as a bard of the
      same level. In addition, he receives bonus spells per day if he has a high
      Charisma score.</p><p>An eldritch scion’s selection of spells is limited.
      He has the same number of spells known as a bard of the same level, and
      can choose new spells to replace old ones at 5th level and every 3 class
      levels after that, just as a bard does. See the bard (Core Rulebook 35)
      for more information on swapping spells known. This replaces the magus’s
      spells class feature.</p><p><strong>Bloodline</strong>: An eldritch scion
      gains a bloodrager bloodline. The bloodline is selected at 1st level, and
      this choice cannot be changed. An eldritch scion’s effective bloodrager
      level for his bloodline abilities is equal to his eldritch scion level. He
      does not gain any bonus feats, and he gains bonus spells from his
      bloodline at different levels (see the bonus spell class feature below).
      To use any ability that normally functions when in a bloodrage, an
      eldritch scion must spend a point from his eldritch pool (see
      below).</p><p>If an eldritch scion takes levels in another class that
      grant a bloodline, the bloodlines must be the same type, even if that
      means that the bloodline of one of the classes must change. Subject to GM
      discretion, an eldritch scion can change his former bloodline to make them
      conform.<br /><br />This ability replaces spell
      recall.</p><p><strong>Eldritch Pool (Su)</strong>: An eldritch scion gains
      an eldritch pool of personal magical energy, equal to 1/2 his magus level
      (minimum 1) + his Charisma modifier. As a swift action, he can spend a
      point of eldritch energy to enter a state of mystical focus for 2 rounds.
      This allows him to use abilities from his bloodrager bloodline as though
      he were in a bloodrage, though he gains none of the other benefits or
      drawbacks of bloodraging. At 4th level, an eldritch scion can also use his
      eldritch pool as an arcane pool, gaining all the benefits listed with the
      magus’s arcane pool class feature.</p><p>Additionally, any magus’s class
      feature or spell from the magus spell list that normally uses a
      calculation based on Intelligence is instead based on Charisma for an
      eldritch scion. For example, an eldritch scion with the arcane accuracy
      magus arcana grants himself an insight bonus on attacks equal to his
      Charisma bonus, not his Intelligence bonus. This has no effect on the
      eldritch scion’s skills or skill points. This ability replaces arcane
      pool, and abilities that modify arcane pool also modify eldritch
      pool.</p><p><strong>Spell Combat (Ex)</strong>: An eldritch scion can only
      use spell combat while in a state of mystic focus (see eldritch pool,
      above). At 8th level, an eldritch scion can use spell combat at any time.
      This ability alters spell combat.</p><p><strong>Bonus Spells</strong>: At
      7th level, an eldritch scion gains the bonus spell from his bloodrager
      bloodline that is normally gained at 10th level. He gains the next three
      bonus spells from his bloodline at 9th, 11th, and 13th levels,
      respectively. This ability replaces knowledge pool.</p><p><strong>Improved
      Spell Combat (Ex)</strong>: At 14th level, an eldritch scion gains the
      improved spell combat class feature. This ability alters improved spell
      combat.</p><p><strong>Greater Spell Combat (Ex)</strong>: At 18th level,
      an eldritch scion gains the greater spell combat class feature. This
      ability alters greater spell combat.</p>
  sources:
    - id: PZO1129
      pages: '104'
  subType: misc
  tags:
    - Eldritch Scion
type: feat

