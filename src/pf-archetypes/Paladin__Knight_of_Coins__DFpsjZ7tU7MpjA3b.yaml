_id: DFpsjZ7tU7MpjA3b
_key: '!items!DFpsjZ7tU7MpjA3b'
folder: tGrYpg6QLydbIBYl
img: systems/pf1/icons/spells/protect-royal-3.jpg
name: Paladin (Knight of Coins)
system:
  associations:
    classes:
      - Paladin
  description:
    value: >-
      <p>Servants of gods who revere wealth, knights of coins promote fair trade
      and stable commerce between cities and between nations. Many of these
      knights are Abadarans, and some have also taken the sacred shield
      archetype. Regardless, knights of coins know that a prosperous city can
      uplift and enrich more people than pious prayers
      alone.</p><p><strong>Class Skills</strong>: A knight of coins adds
      Appraise, Knowledge (local), Perception, and Use Magic Device to her list
      of class skills. She gains a number of skill ranks equal to 4 + her
      Intelligence modifier at each level, instead of a number of skill ranks
      equal to 2 + her Intelligence modifier.</p><p>This alters the paladin’s
      class skills and skill ranks per level.</p><p><strong>Eye for Forgeries
      (Sp)</strong>: At will, a knight of coins can concentrate on a single item
      within 30 feet as a move action. When she does, the knight of coins can
      detect if it is magical, as per the spell detect magic. The knight of
      coins cannot concentrate on more than one item in this way at a
      time.</p><p>This replaces detect evil.</p><p><strong>Blessing of
      Prosperity (Su)</strong>: A knight of coins is a patron of wealth and
      seeks to even the playing field for those who have unfair economic
      disadvantages. At 3rd level and every 6 levels thereafter, the paladin can
      select a blessing (see Blessings below). By expending a use of lay on
      hands, the knight of coins can instead grant a boon to herself or a
      creature touched as a standard action. These boons do not stack with
      themselves or with blessings from another knight of coins. A blessing of
      prosperity lasts up to 1 hour, though the knight of coins who bestowed it
      can end a blessing’s benefits early (whether it affects her or another
      creature) as a free action. Using this ability requires only one free hand
      and is a standard action, unless the knight of coins targets herself, in
      which case it is a swift action.</p><p>If she knows multiple blessings of
      prosperity, the knight of coins can bestow multiple blessings on a single
      target as part of the action and the expenditure of a use of lay on hands
      required to use this ability. If she bestows multiple different blessings,
      these blessings are cumulative. For example, a 9th-level knight of coins’
      blessing of prosperity ability might grant a +4 sacred bonus on Appraise
      and Sense Motive checks as well as a +4 sacred bonus on Perception checks
      and to CMD against disarm and steal combat maneuvers.</p><p>A knight of
      coins who takes the Extra Mercy feat can gain an extra blessing of
      prosperity instead of an extra mercy. Once a blessing of prosperity is
      chosen, it can’t be changed. A character who has benefited from a blessing
      of prosperity cannot benefit from that same blessing again (whether
      bestowed by the same knight of coins or another knight of coins) for 24
      hours.</p><p><strong>Blessings</strong>: The following blessings are
      available to a knight of coins.</p><ul><li>The target adds an extra 10% to
      the gp value gained when selling off treasure (normally 50% of the item’s
      original value). This blessing cannot result in selling treasure for more
      than 100% of its original value.</li><li>The target gains a +4 sacred
      bonus on Appraise and Sense Motive checks.</li><li>The target gains a +4
      sacred bonus on Perception checks and to CMD against disarm and steal
      combat maneuvers.</li><li>The target gains a +4 sacred bonus on Craft,
      Perform, and Profession checks.</li><li>The target can use locate object
      as a spell-like ability once, using the knight of coins’ class level as
      the caster level.</li><li>The target can treat one settlement as having
      its base value and purchase limit increased by 30%. The target must choose
      the affected settlement when the knight of coins bestows this
      blessing.</li><li>The target can treat one settlement as being one size
      category larger for the purposes of determining available magic items. The
      target must choose the affected settlement when the knight of coins
      bestows this blessing.</li></ul><p>This replaces the mercies gained at
      3rd, 9th, and 15th levels.</p>
  sources:
    - id: PZO9489
      pages: '28'
  subType: misc
  tags:
    - Knight of Coins
type: feat

