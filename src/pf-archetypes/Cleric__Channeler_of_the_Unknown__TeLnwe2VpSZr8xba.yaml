_id: TeLnwe2VpSZr8xba
_key: '!items!TeLnwe2VpSZr8xba'
folder: sLBqRj6M4d3kKlvh
img: systems/pf1/icons/items/inventory/badge-cross.jpg
name: Cleric (Channeler of the Unknown)
system:
  associations:
    classes:
      - Cleric
  description:
    value: >-
      <p>While most clerics who fall out of favor with their deities simply lose
      their divine connection and the powers it granted, a few continue to go
      through the motions of prayer and obedience, persisting in the habits of
      faith even when their faith itself has faded. Among these, an even smaller
      number find that while their original deity no longer answers their
      prayers, something else does: an unknown entity or force of the universe
      channeling its power through a trained and practicing vessel.</p><p>This
      is an ex-class archetype and can be taken by a character immediately upon
      becoming an ex-cleric.</p><p></p><p></p><h3>EX-CLASS ARCHETYPES</h3><p>The
      following archetype can be taken by an ex-cleric immediately upon becoming
      an ex-cleric, regardless of character level, replacing some or all of the
      lost class abilities. If another archetype the character had before she
      became an ex-cleric replaces the same ability as the ex-class archetype,
      she loses the old archetype in favor of the new one; otherwise, she can
      retain both archetypes as normal. Channelers of the unknown can gain
      further levels in the cleric class, even though becoming an ex-cleric
      normally prohibits further advancement in the class. While an ex-member of
      a class can recant her failings and atone for her fall from her original
      class (typically involving an atonement spell), her acceptance of her
      ex-class archetype means she must atone both for her initial fall and for
      further straying from the path. As a result, such a character must be the
      target of two atonement spells or a similar effect to regain her lost
      class features. Upon doing so, she immediately loses this archetype and
      regains her original class (and archetype, if she had one).</p><p>Weapon
      and Armor Proficiency : A channeler of the unknown loses proficiency with
      her deity’s favored weapon. She instead gains proficiency with one martial
      or exotic weapon, chosen when she first takes this archetype, which
      thereafter effectively functions as her holy or unholy symbol for the
      purposes of class abilities and spellcasting. The weapon chosen cannot be
      one associated with her former deity. Once she makes this choice, she
      can’t later change it.</p><p>This alters the cleric’s weapon and armor
      proficiency.</p><p><strong>Spells</strong>: A channeler of the unknown has
      one fewer spell slot per spell level in which she can prepare spells than
      normal. She is no longer restricted by alignment descriptors, and she
      gains access to all spells on the cleric spell list, even spells her
      alignment would normally prohibit.</p><p>This alters the cleric’s
      spells.</p><p><strong>Unknown Aura (Su)</strong>: A channeler of the
      unknown never radiates an alignment aura, as if under the effect of a
      permanent undetectable alignment spell.</p><p>This replaces the cleric’s
      aura.</p><p><strong>Channel Entropy (Su)</strong>: A channeler of the
      unknown can channel entropy as a cleric channels negative or positive
      energy, releasing a wave of twisting void that harms creatures in the area
      of effect. The amount of damage dealt is equal to that an evil cleric of
      her level would deal by channeling negative energy, except it affects
      living, unliving, and undead creatures alike. This functions in all other
      ways as a cleric’s channel energy class feature, including benefiting from
      feats that affect channel energy (such as Selective
      Channeling).</p><p>This alters channel energy.</p><p><strong>Power of the
      Unknown</strong>: A channeler of the unknown has lost the benefit of the
      domains granted by her deity, but the unknown entity that answers her
      supplications instead grants her the benefits of one domain from the
      following list: Darkness, Destruction, Luck, Madness, or Void. Instead of
      a single domain spell slot, the channeler of the unknown gains two domain
      spell slots per spell level she can cast. A channeler of the unknown
      cannot select a subdomain in place of the domain available to
      her.</p><p>This alters the cleric’s domains.</p><p><strong>Spontaneous
      Casting</strong>: Instead of converting prepared spells into cure or
      inflict spells, a channeler of the unknown can channel stored spell energy
      into her domain spells. She can lose a prepared spell, including a domain
      spell, to spontaneously cast a domain spell of the same spell level or
      lower.</p><p>This alters spontaneous casting.</p>
  sources:
    - id: PZO9484
      pages: '12'
  subType: misc
  tags:
    - Channeler of the Unknown
type: feat

