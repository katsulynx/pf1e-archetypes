_id: FPBjouGZScqWqtiP
_key: '!items!FPBjouGZScqWqtiP'
img: icons/tools/navigation/map-marked-green.webp
name: Pathfinder Field Agent
system:
  bab: med
  classSkills:
    blf: true
    clm: true
    dev: true
    dip: true
    esc: true
    hea: true
    kar: true
    kdu: true
    ken: true
    kge: true
    khi: true
    klo: true
    kna: true
    kno: true
    kpl: true
    kre: true
    lin: true
    per: true
    ste: true
    swm: true
    umd: true
  description:
    value: >-
      <p>Equipped with a variety of skills and trained to keep a cool head even
      in the most dire circumstances, a practiced Pathfinder field agent is a
      boon to any adventuring party.</p><h2>Requirements</h2><p>To qualify to
      become a Pathfinder field agent, a character must fulfill all the
      following criteria.</p><p><strong>Feats</strong>: Skill Focus (any).<br
      /><strong>Skills</strong>: Knowledge (any) 5 ranks, Linguistics 1 rank,
      Perception 1 rank.<br /><strong>Special</strong>: Must be an active member
      of the Pathfinder Society and own a <em>wayfinder</em>.</p><h2>Class
      Skills</h2><p>The Pathfinder Field Agent's class skills are Bluff (Cha),
      Climb (Str), Diplomacy (Cha), Disable Device (Dex), Escape Artist (Dex),
      Heal (Wis), Knowledge (all) (Int), Linguistics (Int), Perception (Wis),
      Sense Motive (Wis), Stealth (Dex), Swim (Str), and Use Magic Device
      (Cha).</p><p><strong>Skill Points at each Level</strong>: 6 + Int
      modifier.<br /><strong>Hit Die</strong>: d8.</p><h2>Class
      Features</h2><table class="inner" style="width:96.9314%"><tbody><tr><td
      style="width:8.07441%"><strong>Level</strong></td><td
      style="width:18.555%"><strong>Base Attack Bonus</strong></td><td
      style="width:10.4283%"><strong>Fort Save</strong></td><td
      style="width:9.49721%"><strong>Ref Save</strong></td><td
      style="width:10.4283%"><strong>Will Save</strong></td><td
      style="width:42.6443%"><strong>Special</strong></td></tr><tr><td
      style="width:8.07441%">1st</td><td style="width:18.555%">+0</td><td
      style="width:10.4283%">+0</td><td style="width:9.49721%">+1</td><td
      style="width:10.4283%">+1</td><td style="width:42.6443%">Pathfinder
      training, <em>wayfinder</em> upgrade</td></tr><tr><td
      style="width:8.07441%">2nd</td><td style="width:18.555%">+1</td><td
      style="width:10.4283%">+1</td><td style="width:9.49721%">+1</td><td
      style="width:10.4283%">+1</td><td style="width:42.6443%">Bonus
      feat</td></tr><tr><td style="width:8.07441%">3rd</td><td
      style="width:18.555%">+2</td><td style="width:10.4283%">+1</td><td
      style="width:9.49721%">+2</td><td style="width:10.4283%">+2</td><td
      style="width:42.6443%">Pathfinder training</td></tr><tr><td
      style="width:8.07441%">4th</td><td style="width:18.555%">+3</td><td
      style="width:10.4283%">+1</td><td style="width:9.49721%">+2</td><td
      style="width:10.4283%">+2</td><td
      style="width:42.6443%"><em>Wayfinder</em> upgrade</td></tr><tr><td
      style="width:8.07441%">5th</td><td style="width:18.555%">+3</td><td
      style="width:10.4283%">+2</td><td style="width:9.49721%">+3</td><td
      style="width:10.4283%">+3</td><td style="width:42.6443%">Bonus
      feat</td></tr><tr><td style="width:8.07441%">6th</td><td
      style="width:18.555%">+4</td><td style="width:10.4283%">+2</td><td
      style="width:9.49721%">+3</td><td style="width:10.4283%">+3</td><td
      style="width:42.6443%">Pathfinder training</td></tr><tr><td
      style="width:8.07441%">7th</td><td style="width:18.555%">+5</td><td
      style="width:10.4283%">+2</td><td style="width:9.49721%">+4</td><td
      style="width:10.4283%">+4</td><td
      style="width:42.6443%"><em>Wayfinder</em> upgrade</td></tr><tr><td
      style="width:8.07441%">8th</td><td style="width:18.555%">+6</td><td
      style="width:10.4283%">+3</td><td style="width:9.49721%">+4</td><td
      style="width:10.4283%">+4</td><td style="width:42.6443%">Bonus
      feat</td></tr><tr><td style="width:8.07441%">9th</td><td
      style="width:18.555%">+6</td><td style="width:10.4283%">+3</td><td
      style="width:9.49721%">+5</td><td style="width:10.4283%">+5</td><td
      style="width:42.6443%">Pathfinder training</td></tr><tr><td
      style="width:8.07441%">10th</td><td style="width:18.555%">+7</td><td
      style="width:10.4283%">+3</td><td style="width:9.49721%">+5</td><td
      style="width:10.4283%">+5</td><td style="width:42.6443%">Legends
      uncovered, <em>wayfinder</em> upgrade</td></tr></tbody></table><p><br
      />All the following are class features of the Pathfinder field agent
      prestige class.</p><p><strong>Weapon and Armor Proficiency</strong>: A
      Pathfinder field agent gains no additional weapon or armor
      proficiencies.</p><p><strong>Pathfinder Training</strong>: A Pathfinder
      field agent receives additional training that aids her in her fulfilling
      her duty to explore, report, and cooperate. She can’t receive the same
      training more than once unless otherwise noted. A Pathfinder field agent
      gains additional Pathfinder training at 3rd level and every 3 levels
      thereafter.</p><p><em>Animal Magnetism (Ex)</em>: This ability functions
      as the druid’s wild empathy class ability, except the Pathfinder field
      agent’s effective druid level to determine her wild empathy bonus is equal
      to her Pathfinder field agent level. Levels in Pathfinder field agent
      stack with levels from other classes that grant wild empathy for the
      purpose of determining a Pathfinder field agent’s total wild empathy
      bonus.</p><p><em>Deft Defender (Ex)</em>: When the Pathfinder field agent
      successfully uses the aid another action to increase an ally’s Armor
      Class, she gains a +1 dodge bonus to her AC until the start of her next
      turn.</p><p><em>Eidetic Memory (Ex)</em>: Years of training in the
      libraries of countless Pathfinder lodges have given the Pathfinder field
      agent extraordinary recall. A Pathfinder field agent with this training
      adds half her class level (minimum 1) to all Knowledge skill checks and
      can attempt all Knowledge skill checks untrained.</p><p><em>Greater
      Casting</em>: The Pathfinder field agent gains new spells per day as if
      she had also gained a level in a spellcasting class she belonged to before
      adding the prestige class. She doesn’t gain other benefits of that class
      other than spells per day, spells known, and an increased caster level. If
      the Pathfinder field agent has levels in more than one spellcasting class,
      she must choose which class to apply this increase to when she takes this
      Pathfinder training.</p><p><em>Know Thy Enemy (Ex)</em>: The Pathfinder
      field agent can select one creature type, and gains a +5 bonus on
      Knowledge checks to identify creatures of the chosen type. This can be
      taken more than once, and applies to a different creature type each
      time.</p><p><em>Rogue Talent</em>: The Pathfinder field agent can select
      any rogue talent for which she qualifies.</p><p><em>Skill Specialization
      (Ex)</em>: The Pathfinder field agent can select any skill to become a
      class skill for her. In addition, she gains a bonus on checks with the
      chosen skill equal to half her class level. This training can be selected
      more than once, applying to a different skill each time it is
      taken.</p><p><em>Sneak Attack +1d6 (Ex)</em>: This ability is exactly like
      the rogue ability of the same name. If the Pathfinder field agent gains a
      sneak attack bonus from another source, the bonuses on damage stack. A
      Pathfinder field agent must be at least 3rd level before selecting this
      training.</p><p><em>Trapfinding</em>: This training functions as the rogue
      class feature of the same name. Levels in Pathfinder field agent stack
      with levels from other classes that grant this ability for the purpose of
      determining a Pathfinder field agent’s total trapfinding
      bonus.</p><p><strong>Wayfinder Upgrade</strong>: At 1st level and every 3
      levels thereafter, a Pathfinder field agent can add an additional ability
      to her <em>wayfinder</em>. These abilities must be added to a standard
      <em>wayfinder </em>(see <em>Pathfinder Campaign Setting: The Inner Sea
      World Guide</em> 299), not to a variant or modified <em>wayfinder
      </em>(though a Pathfinder field agent can possess more than one
      <em>wayfinder </em>at a time, if she chooses). If a Pathfinder field agent
      loses her upgraded <em>wayfinder</em>, she can purchase a new one at the
      normal cost plus 100 gp for each Pathfinder field agent level she
      possesses, keeping all previously selected upgrades. A Pathfinder field
      agent can choose from the listed <em>wayfinder </em>upgrades. Any upgrades
      that grant additional spell effects use the <em>wayfinder’s</em> caster
      level or the Pathfinder field agent’s class level (whichever is higher) to
      determine effects that depend on caster level.</p><p><em>Dancing
      Lights</em>: The Pathfinder replaces her <em>wayfinder’s</em> ability to
      cast <em>light </em>with the ability to cast<em> dancing lights </em>at
      will.</p><p><em>Daylight</em>: Once per day, the Pathfinder field agent
      can use her <em>wayfinder </em>to cast <em>daylight</em>. This upgrade can
      be selected more than once. Each additional time it’s selected, the
      Pathfinder field agent gains another use of the spell per
      day.</p><p><em>Flask</em>: A hidden compartment is carved into the
      <em>wayfinder’s </em>casing, allowing a single potion, oil, or dose of
      poison to be hidden within. This substance can be retrieved with a swift
      action that doesn’t provoke attacks of opportunity (as opposed to the
      normal move action required to retrieve a stored item). Drinking the
      potion or applying the oil or poison still requires a standard action that
      provokes attacks of opportunity. The Pathfinder field agent gains a +2
      bonus on Sleight of Hand checks made to oppose the Perception check of
      someone observing or frisking her to detect items in the hidden
      compartment.</p><p><em>Hidden</em>: In order to hide her affiliation with
      the Pathfinder Society from potential enemies, a Pathfinder field agent
      with this upgrade can, three times per day, turn her <em>wayfinder
      </em>invisible and mask its magical aura as if it were under the effects
      of <em>invisibility </em>and <em>magic aura</em>. Each time this effect is
      used, it lasts for up to 1 hour per 2 class levels the Pathfinder field
      agent possesses (minimum 1 hour), and can be dismissed by the Pathfinder
      field agent as a move action.</p><p><em>Message</em>: In addition to the
      <em>light </em>spell-like ability, the <em>wayfinder </em>can be used to
      cast <em>message </em>at will. </p><p><em>Shielding</em>: Once per day,
      the Pathfinder field agent can activate her <em>wayfinder </em>to gain a
      +2 deflection bonus to AC for a number of minutes equal to her Pathfinder
      field agent level. This bonus increases by 1 for every 5 class levels the
      Pathfinder field agent possesses. </p><p><em>Silence</em>: Once per day,
      the Pathfinder field agent can use her <em>wayfinder </em>to emit an aura
      of <em>silence</em>, as the spell. The <em>silence</em> effect must be
      centered on the <em>wayfinder</em>. This upgrade can be selected more than
      once. Each additional time the upgrade is selected, the agent gains
      another use of the spell per day.</p><p><em>Stabilize</em>: The wayfinder
      can be used to cast <em>stabilize </em>at will.</p><p><strong>Bonus
      Feat</strong>: At 2nd level and every 3 levels thereafter, a Pathfinder
      field agent can select a bonus feat. Each of these bonus feats must be
      Skill Focus or a teamwork feat. The Pathfinder field agent must meet the
      prerequisites of the selected bonus feat.</p><p><strong>Legends Uncovered
      (Su)</strong>: At 10th level, a Pathfinder field agent has the ability to
      uncover the secrets of the distant past. Once per day when studying or
      researching a person, place, or artifact of antiquity, the agent can
      dedicate herself to the task to gain benefits as though she were casting
      the spell <em>legend lore</em>.</p>
  links:
    classAssociations:
      - level: 1
        name: Wayfinder Upgrade
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.ciyImu4uU8FDRucB
      - level: 1
        name: Pathfinder Training
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.awikzWDddpbaic29
      - level: 2
        name: Bonus Feat
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Mr6GVy8LAe5sNi7v
      - level: 10
        name: Legends Uncovered (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.3MTERYJoDdLsOZo6
  savingThrows:
    ref:
      value: high
    will:
      value: high
  skillsPerLevel: 6
  sources:
    - id: PZO9435
      pages: '18'
  subType: prestige
  tag: pathfinderFieldAgent
type: class

