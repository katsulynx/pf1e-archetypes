_id: YEQI3iYe5GZ37LRQ
_key: '!items!YEQI3iYe5GZ37LRQ'
img: systems/pf1/icons/spells/beam-sky-3.jpg
name: Razmiran Priest
system:
  bab: med
  classSkills:
    blf: true
    dip: true
    int: true
    kre: true
    umd: true
  description:
    value: >-
      <p>Those who live within the boundaries of Razmir’s holy lands know the
      power of the Living God. He makes his presence felt through demands of
      fealty, ever-increasing tithes, and the less-than-gentle hands of his most
      devout subjects. Of these devotees, it is Razmir’s clergy that most
      accurately represents the fervor of their lord. They roam the lands
      spreading word of the power of Razmir, and using that power to convince
      the unfaithful of their misplaced loyalties in other gods and
      governments.</p><p>The Razmiran priest, one of the most powerful of
      Razmir’s servants, brings the Living God’s gifts to the common folk and
      his wrath to his enemies. From behind his mask, the priest judges all he
      sees, meting out harsh justice in the name of his deity. Many doubt the
      powers of the clergy, and some would even dare to call Razmir “false.”
      These fools soon learn the error of their ways when the Razmiran priest
      pays them a visit, using the gifts of the god to smite such
      heretics.</p><p>A devotee who wishes to become one of Razmir’s most
      trusted priests must undergo a lengthy ritual in the capital of
      Thronestep. It is said that Razmir himself conducts the final steps of the
      ritual, blessing the new priest and judging his purity. Those who are
      found worthy emerge from this secret, mysterious process as absolute
      believers, while those deemed inadequate are never seen again. A new
      Razmiran priest finds himself on a quick path to power, prestige, and
      wealth, as the hierarchy of Razmir’s faith treats most priests as
      superiors. Indeed, many Razmiran priests find themselves quickly rising in
      rank to become Heralds of the Eighth Step or possessors of even greater
      power in short order.</p><h2>Requirements</h2><p>To qualify to become a
      Razmiran priest, a character must fulfill all of the following
      criteria.</p><p><strong>Alignment</strong>: Any nongood.<br
      /><strong>Feat</strong>: False Casting (<em>Inner Sea Magic </em>10).<br
      /><strong>Skills</strong>: Bluff 5 ranks, Use Magic Device 5 ranks.<br
      /><strong>Spells</strong>: Able to cast 3rd-level arcane spells.<br
      /><strong>Special</strong>: Must travel to Thronestep to undergo a special
      ritual whereby the character’s faith in Razmir will be
      tested.</p><h2>Class Skills</h2><p>The Razmiran Priest's class skills are
      Bluff (Cha), Diplomacy (Cha), Intimidate (Cha), Knowledge (religion)
      (Int), Sense Motive (Wis), and Use Magic Device (Cha).</p><p><strong>Skill
      Points at each Level</strong>: 4 + Int modifier.<br /><strong>Hit
      Die</strong>: d8.</p><h2>Class Features</h2><table class="inner"
      style="width:96.9314%"><tbody><tr><td
      style="width:8.65134%"><strong>Level</strong></td><td
      style="width:12.5777%"><strong>Base Attack Bonus</strong></td><td
      style="width:7.82123%"><strong>Fort Save</strong></td><td
      style="width:6.89013%"><strong>Ref Save</strong></td><td
      style="width:7.63501%"><strong>Will Save</strong></td><td
      style="width:27.1881%"><strong>Special</strong></td><td
      style="width:28.8641%"><strong>Spells Per Day</strong></td></tr><tr><td
      style="width:8.65134%">1st</td><td style="width:12.5777%">+0</td><td
      style="width:7.82123%">+0</td><td style="width:6.89013%">+0</td><td
      style="width:7.63501%">+1</td><td style="width:27.1881%">Domain of Razmir,
      first ritual</td><td style="width:28.8641%">—</td></tr><tr><td
      style="width:8.65134%">2nd</td><td style="width:12.5777%">+1</td><td
      style="width:7.82123%">+1</td><td style="width:6.89013%">+1</td><td
      style="width:7.63501%">+1</td><td style="width:27.1881%">False channel
      1d6</td><td style="width:28.8641%">+1 level of arcane spellcasting
      class</td></tr><tr><td style="width:8.65134%">3rd</td><td
      style="width:12.5777%">+2</td><td style="width:7.82123%">+1</td><td
      style="width:6.89013%">+1</td><td style="width:7.63501%">+2</td><td
      style="width:27.1881%">Master of lies</td><td style="width:28.8641%">+1
      level of arcane spellcasting class</td></tr><tr><td
      style="width:8.65134%">4th</td><td style="width:12.5777%">+3</td><td
      style="width:7.82123%">+1</td><td style="width:6.89013%">+1</td><td
      style="width:7.63501%">+2</td><td style="width:27.1881%">False channel
      2d6</td><td style="width:28.8641%">+1 level of arcane spellcasting
      class</td></tr><tr><td style="width:8.65134%">5th</td><td
      style="width:12.5777%">+3</td><td style="width:7.82123%">+2</td><td
      style="width:6.89013%">+2</td><td style="width:7.63501%">+3</td><td
      style="width:27.1881%">Domain of Razmir, second ritual</td><td
      style="width:28.8641%">—</td></tr><tr><td
      style="width:8.65134%">6th</td><td style="width:12.5777%">+4</td><td
      style="width:7.82123%">+2</td><td style="width:6.89013%">+2</td><td
      style="width:7.63501%">+3</td><td style="width:27.1881%">False channel
      3d6</td><td style="width:28.8641%">+1 level of arcane spellcasting
      class</td></tr><tr><td style="width:8.65134%">7th</td><td
      style="width:12.5777%">+5</td><td style="width:7.82123%">+2</td><td
      style="width:6.89013%">+2</td><td style="width:7.63501%">+4</td><td
      style="width:27.1881%">Conversion</td><td style="width:28.8641%">+1 level
      of arcane spellcasting class</td></tr><tr><td
      style="width:8.65134%">8th</td><td style="width:12.5777%">+6</td><td
      style="width:7.82123%">+3</td><td style="width:6.89013%">+3</td><td
      style="width:7.63501%">+4</td><td style="width:27.1881%">False channel
      4d6</td><td style="width:28.8641%">+1 level of arcane spellcasting
      class</td></tr><tr><td style="width:8.65134%">9th</td><td
      style="width:12.5777%">+6</td><td style="width:7.82123%">+3</td><td
      style="width:6.89013%">+3</td><td style="width:7.63501%">+5</td><td
      style="width:27.1881%">Third ritual</td><td
      style="width:28.8641%">—</td></tr><tr><td
      style="width:8.65134%">10th</td><td style="width:12.5777%">+7</td><td
      style="width:7.82123%">+3</td><td style="width:6.89013%">+3</td><td
      style="width:7.63501%">+5</td><td style="width:27.1881%">False channel
      5d6, true pretender</td><td style="width:28.8641%">+1 level of arcane
      spellcasting class</td></tr></tbody></table><p><br />All the following are
      class features of the Razmiran priest prestige class.</p><p><strong>Weapon
      and Armor Proficiency</strong>: A Razmiran priest gains no additional
      weapon or armor proficiencies.</p><p><strong>Domain of Razmir</strong>: A
      Razmiran priest may select one of the following domains: Charm, Evil, Law,
      or Trickery. A Razmiran priest gains the granted powers of that domain,
      using his arcane spellcaster level as his cleric level and his
      Intelligence or Charisma modifier (whichever is higher) in place of his
      Wisdom modifier. He does not gain any domain spells from this
      domain.</p><p>At 5th level, a Razmiran priest may select a second domain
      from those listed above and may use the granted powers of that domain as
      well.</p><p><strong>First Ritual (Su)</strong>: At 1st level, a Razmiran
      adds both <em>bless</em> and <em>cure light wounds </em>to one of his
      arcane spell lists, but treats them as if they were 2nd-level spells. If
      he is a spontaneous caster, they are also added to his list of spells
      known. Whenever he casts <em>cure light wounds </em>using this ability,
      the hit points healed are instead treated as temporary hit points that
      last 10 minutes per Razmiran priest level, although the subject of the
      spell does not notice this difference until the duration expires.
      Temporary hit points from this (and any subsequent cure spells) do not
      stack.</p><p>In addition, whenever a Razmiran priest uses False Casting to
      cast a 0- or 1st-level spell on the cleric spell list, he receives a +10
      circumstance bonus on any opposed Bluff check made when casting the
      spell.</p><p><strong>False Channel (Su)</strong>: Starting at 2nd level, a
      Razmiran priest can channel positive energy to heal living creatures like
      a cleric, healing 1d6 points of damage, plus 1d6 points of damage for
      every two Razmiran priest levels after 2nd. He can use this ability a
      number of times per day equal to 3 + his Intelligence or Charisma modifier
      (whichever is higher). The hit points healed by this ability are treated
      as temporary hit points, and last for 10 minutes per Razmiran priest
      level. These hit points do not stack with themselves (although they do
      stack with those from <em>cure light wounds</em> cast using the first
      ritual class feature). This ability does not count for the purposes of
      meeting any prerequisites that call for the ability to channel energy.
      This ability cannot be used to harm undead creatures.</p><p><strong>Master
      of Lies (Ex)</strong>: At 3rd level, a Razmiran priest adds his class
      level to any Bluff check made as part of a lie or deception. In addition,
      whenever he is subject to a spell or special ability that can discern
      whether or not he is telling the truth, he is automatically aware of such
      magic. He is not immune to such spells or abilities, but he can choose to
      remain silent or avoid answering questions.</p><p><strong>Second Ritual
      (Su)</strong>: At 5th level, a Razmiran priest adds <em>cure moderate
      wounds</em> and <em>spiritual weapon </em>to one of his arcane spell
      lists, but treats them as if they were 3rd-level spells. If he is a
      spontaneous caster, they are also added to his list of spells known. As
      with the first ritual, any hit points gained from <em>cure moderate wounds
      </em>are temporary hit points. Finally, whenever a Razmiran priest uses
      False Casting to cast a 2nd- or 3rd-level spell on the cleric spell list,
      he receives a +10 circumstance bonus on any opposed Bluff check made when
      casting the spell.</p><p><strong>Conversion (Su)</strong>: At 7th level,
      whenever a Razmiran priest heals himself using false channel, the effect
      heals damage, instead of granting temporary hit points. This has no effect
      on others in the area of effect.</p><p><strong>Third Ritual (Su)</strong>:
      At 9th level, a Razmiran priest adds <em>cure serious wounds </em>and
      <em>prayer </em>to one of his arcane spell lists, but treats them as if
      they were 4th-level spells. If he is a spontaneous caster, they are also
      added to his list of spells known. As with the first ritual, any hit
      points gained from <em>cure serious wounds </em>are temporary hit points.
      Finally, whenever a Razmiran priest uses False Casting to cast a 4th- or
      5th-level spell on the cleric spell list, he receives a +10 circumstance
      bonus on any opposed Bluff check made when casting the
      spell.</p><p><strong>True Pretender (Su)</strong>: At 10th level, a
      Razmiran priest attains true power. Whenever he uses false channel, he can
      spend two uses of that ability to heal damage to all living creatures in
      the area, instead of granting temporary hit points. In addition, when
      determining what spell trigger and spell completion magic items he can
      use, he acts as though all cleric spells were on his spell list. This
      makes it so that he does not need to make a Use Magic Device skill check
      when using such items, but he must still make Bluff skill checks when
      using False Casting with such spells.</p>
  links:
    classAssociations:
      - level: 1
        name: Domain of Razmir
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.0g7AZD0jFcwoWQJi
      - level: 1
        name: First Ritual (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.5rIcn7X0y78TqKor
      - level: 2
        name: False Channel (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.2RISH43xwfRAVvrV
      - level: 3
        name: Master of Lies (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.8L0STp1QlPN2n5m8
      - level: 5
        name: Second Ritual (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.3BofvEJKeLJa2bxJ
      - level: 7
        name: Conversion (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.3FufkpC8iQ3kGZYg
      - level: 9
        name: Third Ritual (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.mcOTEtvfbuYaDnxx
      - level: 10
        name: True Pretender (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.rzyMmrd14egz5UrV
  savingThrows:
    will:
      value: high
  skillsPerLevel: 4
  sources:
    - id: PZO9249
      pages: '44'
  subType: prestige
  tag: razmiranPriest
type: class

