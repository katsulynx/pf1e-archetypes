_id: jwvZYZeCuUgI4AbD
_key: '!items!jwvZYZeCuUgI4AbD'
img: systems/pf1/icons/skills/yellow_22.jpg
name: Runeguard
system:
  classSkills:
    apr: true
    crf: true
    hea: true
    kar: true
    kdu: true
    ken: true
    kge: true
    khi: true
    klo: true
    kna: true
    kno: true
    kpl: true
    kre: true
    lin: true
    spl: true
    umd: true
  description:
    value: >-
      <p>The ancient traditions of Thassilonian magic became mired in the
      culture of sin due to the legacy of that land’s runelords, but taken in
      its purest form, Thassilonian magic is no more good than evil. Indeed, at
      Thassilon’s outset—before the runelords seized control—the original
      incarnations of these seven schools of magic were not associated with sins
      at all, but rather with the seven virtues of rule. This original lore is
      all but lost, but within the city-states of Varisia (particularly in
      Magnimar), a small order of spellcasters seeks to reclaim the knowledge of
      those empowering virtues. With the might of virtuous runes, they protect
      their homes from the lingering evil of Thassilon and look to the guidance
      of Soralyon, the Mystic Angel, to aid them in their quest. These
      spellcasters have also come to believe that a runelord’s return to power
      is practically an inevitable threat that must be prepared for and defended
      against.</p><p>The role of a runeguard varies depending on the type of
      spellcasting he employs, but most tend to be more defensive rather than
      offensive.</p><h2>Requirements</h2><p>To qualify to become a runeguard, a
      character must fulfill all of the following
      criteria.</p><p><strong>Alignment</strong>: Lawful good, neutral good, or
      chaotic good. <br /><strong>Deity</strong>: Must worship Soralyon. <br
      /><strong>Skills</strong>: Linguistics 5 ranks, Spellcraft 5 ranks. <br
      /><strong>Feats</strong>: Scribe Scroll. <br /><strong>Language</strong>:
      Thassilonian. <br /><strong>Spells</strong>: Able to cast 3rd-level
      spells.</p><h2>Class Skills</h2><p>The Runeguard's class skills are
      Appraise (Int), Heal (Wis), Knowledge (all skills taken individually)
      (Int), Linguistics (Int), Spellcraft (Int), and Use Magic Device
      (Cha).</p><p><strong>Skill Points at each Level</strong>: 2 + Int
      modifier.<br /><strong>Hit Die</strong>: d6.</p><h2>Class
      Features</h2><table class="inner" style="width:96.9314%"><tbody><tr><td
      style="width:7.30518%"><strong>Level</strong></td><td
      style="width:14.8549%"><strong>Base Attack Bonus</strong></td><td
      style="width:8.19367%"><strong>Fort Save</strong></td><td
      style="width:7.26257%"><strong>Ref Save</strong></td><td
      style="width:8.00745%"><strong>Will Save</strong></td><td
      style="width:22.3464%"><strong>Special</strong></td><td
      style="width:31.6574%"><strong>Spells Per Day</strong></td></tr><tr><td
      style="width:7.30518%">1st</td><td style="width:14.8549%">+0</td><td
      style="width:8.19367%">+0</td><td style="width:7.26257%">+0</td><td
      style="width:8.00745%">+1</td><td style="width:22.3464%">Rune magic,
      virtuous rune</td><td style="width:31.6574%">—</td></tr><tr><td
      style="width:7.30518%">2nd</td><td style="width:14.8549%">+1</td><td
      style="width:8.19367%">+1</td><td style="width:7.26257%">+1</td><td
      style="width:8.00745%">+1</td><td style="width:22.3464%">Virtuous
      rune</td><td style="width:31.6574%">+1 level of existing spellcasting
      class</td></tr><tr><td style="width:7.30518%">3rd</td><td
      style="width:14.8549%">+1</td><td style="width:8.19367%">+1</td><td
      style="width:7.26257%">+1</td><td style="width:8.00745%">+2</td><td
      style="width:22.3464%">Rune focus +1</td><td style="width:31.6574%">+1
      level of existing spellcasting class</td></tr><tr><td
      style="width:7.30518%">4th</td><td style="width:14.8549%">+2</td><td
      style="width:8.19367%">+1</td><td style="width:7.26257%">+1</td><td
      style="width:8.00745%">+2</td><td style="width:22.3464%">Virtuous
      rune</td><td style="width:31.6574%">+1 level of existing spellcasting
      class</td></tr><tr><td style="width:7.30518%">5th</td><td
      style="width:14.8549%">+2</td><td style="width:8.19367%">+2</td><td
      style="width:7.26257%">+2</td><td style="width:8.00745%">+3</td><td
      style="width:22.3464%">Virtuous rune</td><td style="width:31.6574%">+1
      level of existing spellcasting class</td></tr><tr><td
      style="width:7.30518%">6th</td><td style="width:14.8549%">+3</td><td
      style="width:8.19367%">+2</td><td style="width:7.26257%">+2</td><td
      style="width:8.00745%">+3</td><td style="width:22.3464%">Virtuous
      rune</td><td style="width:31.6574%">+1 level of existing spellcasting
      class</td></tr><tr><td style="width:7.30518%">7th</td><td
      style="width:14.8549%">+3</td><td style="width:8.19367%">+2</td><td
      style="width:7.26257%">+2</td><td style="width:8.00745%">+4</td><td
      style="width:22.3464%">Rune focus +2</td><td style="width:31.6574%">+1
      level of existing spellcasting class</td></tr><tr><td
      style="width:7.30518%">8th</td><td style="width:14.8549%">+4</td><td
      style="width:8.19367%">+3</td><td style="width:7.26257%">+3</td><td
      style="width:8.00745%">+4</td><td style="width:22.3464%">Virtuous
      rune</td><td style="width:31.6574%">+1 level of existing spellcasting
      class</td></tr><tr><td style="width:7.30518%">9th</td><td
      style="width:14.8549%">+4</td><td style="width:8.19367%">+3</td><td
      style="width:7.26257%">+3</td><td style="width:8.00745%">+5</td><td
      style="width:22.3464%">Virtuous rune</td><td style="width:31.6574%">+1
      level of existing spellcasting class</td></tr><tr><td
      style="width:7.30518%">10th</td><td style="width:14.8549%">+5</td><td
      style="width:8.19367%">+3</td><td style="width:7.26257%">+3</td><td
      style="width:8.00745%">+5</td><td style="width:22.3464%">Sihedron rune
      mastery</td><td style="width:31.6574%">+1 level of existing spellcasting
      class</td></tr></tbody></table><p><br />Below are the class features of
      the runeguard prestige class.</p><p><strong>Rune Magic</strong>: A
      runeguard adds all spells with “glyph” or “symbol” in its name to his
      class spell list.</p><p><strong>Virtuous Rune (Su)</strong>: At 1st level,
      a runeguard can master a secret method of using one of the seven runes of
      Thassilonian magic in a beneficial way to aid himself or others. He must
      choose one of the seven virtues when he gains this ability, but can choose
      another at 2nd, 4th, 5th, 6th, 8th, and 9th levels; by 9th level, he has
      mastered all seven of the secrets of virtuous runes. A runeguard can use
      any of the virtuous runes that he has mastered in any combination per day,
      but no more times per day than his runeguard level overall. Using a
      virtuous rune is a standard action (unless otherwise indicated in the
      text) and provokes an attack of opportunity.</p><p><em>Charity</em>: A
      runeguard can use the rune of charity to transfer a single abjuration
      spell he has prepared or knows (if he’s a spontaneous caster), along with
      the ability to cast it, to a willing creature by touch, as per imbue with
      spell ability, save that the recipient’s Hit Dice do not limit options.
      The level of the spell being transferred can’t exceed the runeguard’s
      level – 1 (and thus a 1st level runeguard can only transfer a 0-level
      spells in this way). Once the spell is transferred, that spell slot
      remains unavailable to the runeguard until the creature that gained the
      spell casts it, at which point the runeguard regains access to the spell
      slot the next time he rests and prepares his
      magic.</p><p><em>Kindness</em>: When using the Heal skill to treat deadly
      wounds, the runeguard can call upon the rune of kindness to restore double
      the normal amount of hit points he otherwise would have healed by treating
      deadly wounds. He need not expend uses from a healer’s kit when treating
      deadly wounds in this manner, and using this rune does not count against
      the total number of times a creature can benefit from heaving deadly
      wounds treated in a day.</p><p><em>Generosity</em>: When using the aid
      another action, the runeguard can draw upon the rune of generosity and
      expend a prepared spell or spell slot as an immediate action to grant an
      ally an insight bonus on attack rolls, on skill checks, or to Armor Class
      equal to the level of spell expended. The bonus persists for a number of
      rounds equal to the runeguard’s class level.</p><p><em>Humility</em>: When
      the runeguard gains the benefits of the aid another action from an ally,
      he can expend a prepared spell or a spell slot (for a spontaneous caster)
      as an immediate action to gain an insight bonus on attack rolls, on skill
      checks, or to Armor Class equal to the level of the spell expended. This
      bonus lasts for a number of rounds equal to the runeguard’s class
      level.</p><p><em>Love</em>: By sacrificing a prepared spell or a spell
      slot (for a spontaneous caster), a runeguard can use the rune of love to
      form a close and powerful bond with a number of willing allies equal to
      the level of the spell or spell slot sacrificed. All allies to be affected
      must be within 30 feet at the time the rune is used. Until the next time
      the runeguard prepares spells (at which point he regains the sacrificed
      spell or spell slot), each ally can sense the general emotional state,
      health, and direction of any other ally bearing the rune (as per status),
      as long as they are on the same plane. If a spell slot of 5th level or
      higher is sacrificed, the link also grants a telepathic bond (as per the
      spell) that can’t be dispelled. A character with the rune of love active
      gains a +2 bonus on all saving throws against charm and compulsion
      effects.</p><p><em>Temperance</em>: By invoking the rune of temperance,
      the runeguard becomes immune to the effects of negative energy for 1 round
      per runeguard level. The runeguard can use this ability as an immediate
      action to gain immunity to the effects of a single negative energy effect
      as that effect targets him, but doing so leaves the runeguard staggered on
      his next action.</p><p><em>Zeal</em>: By invoking the rune of zeal as a
      swift action, the runeguard bolsters his spellcasting for 1 round. During
      this time, the runeguard gains a bonus on all concentration checks equal
      to his runeguard level, and he can enhance any spell he casts that round
      with the effects of any metamagic feat he knows, provided the metamagic
      feat only uses up a spell slot 1 level higher than the spell’s actual
      level.</p><p><strong>Rune Focus</strong>: At 3rd level, the runeguard adds
      1 to the DCs of saving throws against glyph or symbol spells (any spell
      with “glyph” or “symbol” in its name) and spells that are language
      dependent that he casts. This bonus stacks with the bonuses from the Spell
      Focus and Greater Spell Focus feats. At 7th level, the runeguard adds 2 to
      the DCs of saving throws against these spells that he
      casts.</p><p><strong>Sihedron Rune Mastery (Su)</strong>: At 10th level, a
      runeguard can cast <em>permanency</em> as a spell-like ability once per
      day by drawing upon the power of the Sihedron rune. He must still provide
      the spell to be made permanent by other means, of course. Although a
      runeguard can use this ability once per day, he can only ever maintain a
      single spell effect, and upon using this ability, the previous permanent
      spell effect immediately ends.</p>
  hd: 6
  hp: 6
  links:
    classAssociations:
      - level: 1
        name: Virtuous Rune (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.Vh3QCBA3W76CzMoP
      - level: 3
        name: Rune Focus
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.o0aOkxFpFtTLfe15
      - level: 10
        name: Sihedron Rune Mastery (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.xVmTXd3bogxVCH9a
  savingThrows:
    will:
      value: high
  skillsPerLevel: 2
  sources:
    - id: PZO9474
      pages: '22'
  subType: prestige
  tag: runeguard
type: class

