_id: MB2DGZv3h5w71MY7
_key: '!items!MB2DGZv3h5w71MY7'
img: systems/pf1/icons/skills/yellow_08.jpg
name: Chevalier
system:
  bab: high
  classSkills:
    blf: true
    crf: true
    dip: true
    int: true
    prf: true
    pro: true
    sur: true
  description:
    value: >-
      <p><strong>Source</strong>: Pathfinder #14: Children of the Void pg. 62<br
      />Some heroes don&rsquo;t believe in fancy rules or high creeds or big
      flowery speeches, they just want to kill evil things, spend time with
      friends, and swap great tales over a good meal and tasty drinks. Not as
      stuffy or restrictive as a fullfledged knighthood, in their hearts
      chevaliers are just good people who want to celebrate the good things in
      life and fight evil wherever it dares to tread.</p><p>Chevaliers welcome
      all like-minded folk regardless of religion, though most of them worship
      Cayden Cailean. Most are fighters, rogues, or barbarians, but sorcerers,
      clerics, and bards are not unheard of. Chevaliers often wear golden pins
      noting their patron liege or deity or the token of a lover or other for
      whom they fight. Membership is easily obtained by swearing an oath in the
      presence of another chevalier and invoking the name of who they fight for
      or the figure that embodies their desire to do good in the world. An
      adventurer-turned- innkeeper in a frontier town, a band of travelers
      looking for bandits, and a would-be dragonslayer might all be chevaliers.
      They mix lofty intentions, unusual combat techniques, and stubborn
      determination into a strangely effective
      combination.</p><h2>Requirements</h2><p>To qualify to become a chevalier,
      a character must fulfill all of the following
      criteria:</p><p><strong>Alignment</strong>: Good. Most are neutral good or
      chaotic good, with a rare few lawful good.<br /><strong>Base Attack
      Bonus</strong>: +6.<br /><strong>Skills</strong>: Gather Information 4
      ranks, Knowledge (local) 4 ranks.<br /><strong>Special</strong>: A
      would-be chevalier must have succeeded at a challenge requiring great
      heroism; for example, they must participate in an encounter with an EL at
      least 3 levels greater than their level. (Being carried through an
      encounter by a group of more experienced heroes is not
      courageous.)</p><h2>Class Skills</h2><p>The Chevalier's class skills are
      Balance, Bluff, Craft, Diplomacy, Gather Information, Intimidate, Perform,
      Profession, Survival, Tumble.</p><p><strong>Skill Points at each
      Level</strong>: 4 + Int modifier.<br /><strong>Hit Die</strong>:
      d10.</p><h2>Class Features</h2><table class="inner" style="width:
      96.9314%;"><tbody><tr><td style="width:
      7.68371%;"><strong>Level</strong></td><td style="width:
      20.4354%;"><strong>Base Attack Bonus</strong></td><td style="width:
      11.1732%;"><strong>Fort Save</strong></td><td style="width:
      10.0559%;"><strong>Ref Save</strong></td><td style="width:
      10.987%;"><strong>Will Save</strong></td><td style="width:
      39.2924%;"><strong>Special</strong></td></tr><tr><td style="width:
      7.68371%;">1st</td><td style="width: 20.4354%;">+1</td><td style="width:
      11.1732%;">+2</td><td style="width: 10.0559%;">+0</td><td style="width:
      10.987%;">+2</td><td style="width: 39.2924%;">Aura of courage,
      recklessness</td></tr><tr><td style="width: 7.68371%;">2nd</td><td
      style="width: 20.4354%;">+2</td><td style="width: 11.1732%;">+3</td><td
      style="width: 10.0559%;">+0</td><td style="width: 10.987%;">+3</td><td
      style="width: 39.2924%;">Controlled charge, stubborn mind</td></tr><tr><td
      style="width: 7.68371%;">3rd</td><td style="width: 20.4354%;">+3</td><td
      style="width: 11.1732%;">+3</td><td style="width: 10.0559%;">+1</td><td
      style="width: 10.987%;">+3</td><td style="width: 39.2924%;">Poison
      immunity, smite
      evil</td></tr><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr><tr></tr></tbody></table><p><br
      />The following are class features of the chevalier prestige class.<br
      /><br /><strong>Aura of Courage (Su)</strong>: A chevalier has an aura of
      courage like that of a 3rd-level paladin.</p><p><strong>Recklessness
      (Ex)</strong>: Sometimes hasty action proves more useful than even the
      best laid plans. A chevalier gains a morale bonus equal to his class level
      on attack and damage rolls on the round he enters a battle. A chevalier
      only gains this bonus against an opponent (or group of opponents) once per
      day. Thus, he cannot leave a battle and reengage moments later to gain the
      benefits of this ability more than once in the same
      battle.</p><p><strong>Controlled Charge (Ex)</strong>: At 2nd level, a
      chevalier no longer takes a &ndash;2 penalty to AC when
      charging.</p><p><strong>Stubborn Mind (Ex)</strong>: At 3rd level, a
      chevalier gains an incredibly stubborn determination. If he is affected by
      an enchantment spell or effect and fails his saving throw, he can attempt
      it again 1 round later at the same DC. He gets only this one extra chance
      to succeed on his saving throw (though this does not prevent him from
      using other means to break the effect, such as a rogue&rsquo;s slippery
      mind ability).</p><p><strong>Poison Immunity (Ex)</strong>: At 3rd level,
      a chevalier becomes immune to poison.</p><p><strong>Smite Evil
      (Su)</strong>: Once per day, a chevalier can smite evil as if he were a
      paladin of his character level.</p>
  hd: 10
  hp: 10
  links:
    classAssociations:
      - level: 1
        name: Aura of Courage (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.RFu1MEABBOupnzD0
      - level: 1
        name: Recklessness (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.60CG4wbViDiKY9RX
      - level: 2
        name: Controlled Charge (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.LzY5SyQjQkK0GyIL
      - level: 2
        name: Stubborn Mind (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.0pNrw7ZZnw5r8rNK
      - level: 3
        name: Smite Evil (Su)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.N47bQ5Y2W5KMfQI1
      - level: 3
        name: Poison Immunity (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.msXOaIcCIzWkSpTe
  savingThrows:
    fort:
      value: high
    will:
      value: high
  skillsPerLevel: 4
  subType: prestige
  tag: chevalier
type: class

