_id: uHdrA3dprsnLlUll
_key: '!items!uHdrA3dprsnLlUll'
img: icons/equipment/head/hat-tricorn-pirate-black.webp
name: Inner Sea Pirate
system:
  bab: med
  classSkills:
    apr: true
    clm: true
    int: true
    kna: true
    pro: true
    swm: true
  description:
    value: >-
      <p>Pirates of the Inner Sea region sail many different bodies of water in
      search of plunder, from the most common Shackles pirates to the privateers
      of Andoran and the fast-moving brigands of the River Kingdoms. Those who
      prove handy on a ship might rise to the rank of captain and command their
      own ships. Inner Sea pirates are adept at both sailing and sword fighting,
      and most have looked death in the face more than once. They possess a
      variety of skills that prove useful when sailing and plundering, and two
      Inner Sea pirates might be quite different from each
      other.</p><p><strong>Role</strong>: While many sailors in the waters
      around Avistan and Garund work as pirates, those experienced enough to
      take levels in this prestige class show more talent than do common deck
      hands. Inner Sea pirates often captain their own ships; those who do not
      usually serve as officers or aboard notorious ships under accomplished
      captains.</p><p><strong>Alignment</strong>: Inner Sea pirates are often
      chaotic and rarely good, but can be any nonlawful
      alignment.</p><h2>Requirements</h2><p>To qualify to become an Inner Sea
      pirate, a character must fulfill all of the following
      criteria.</p><p><strong>Alignment</strong>: Any nonlawful.<br
      /><strong>Skills</strong>: Appraise 5 ranks, Profession (sailor) 5 ranks,
      Swim 5 ranks.</p><h2>Class Skills</h2><p>The Inner Sea Pirate's class
      skills are Appraise (Int), Climb (Str), Intimidate (Cha), Knowledge
      (nature) (Int), Profession (sailor) (Wis), and Swim (Str).<br /><br
      /><strong>Skill Points at each Level</strong>: 6 + Int modifier.<br
      /><strong>Hit Die</strong>: d8.</p><h2>Class Features</h2><table
      class="inner" style="width:96.9314%"><tbody><tr><td
      style="width:9.78692%"><strong>Level</strong></td><td
      style="width:20.1945%"><strong>Base Attack Bonus</strong></td><td
      style="width:11.9181%"><strong>Fort Save</strong></td><td
      style="width:10.8007%"><strong>Ref Save</strong></td><td
      style="width:11.7318%"><strong>Will Save</strong></td><td
      style="width:35.1955%"><strong>Special</strong></td></tr><tr><td
      style="width:9.78692%">1st</td><td style="width:20.1945%">+0</td><td
      style="width:11.9181%">+0</td><td style="width:10.8007%">+1</td><td
      style="width:11.7318%">+0</td><td style="width:35.1955%">Sneak attack
      +1d6</td></tr><tr><td style="width:9.78692%">2nd</td><td
      style="width:20.1945%">+1</td><td style="width:11.9181%">+1</td><td
      style="width:10.8007%">+1</td><td style="width:11.7318%">+1</td><td
      style="width:35.1955%">Pirate trick</td></tr><tr><td
      style="width:9.78692%">3rd</td><td style="width:20.1945%">+2</td><td
      style="width:11.9181%">+1</td><td style="width:10.8007%">+2</td><td
      style="width:11.7318%">+1</td><td style="width:35.1955%">Pirate
      trick</td></tr><tr><td style="width:9.78692%">4th</td><td
      style="width:20.1945%">+3</td><td style="width:11.9181%">+1</td><td
      style="width:10.8007%">+2</td><td style="width:11.7318%">+1</td><td
      style="width:35.1955%">Sneak attack +2d6</td></tr><tr><td
      style="width:9.78692%">5th</td><td style="width:20.1945%">+3</td><td
      style="width:11.9181%">+2</td><td style="width:10.8007%">+3</td><td
      style="width:11.7318%">+2</td><td style="width:35.1955%">Pirate
      trick</td></tr><tr><td style="width:9.78692%">6th</td><td
      style="width:20.1945%">+4</td><td style="width:11.9181%">+2</td><td
      style="width:10.8007%">+3</td><td style="width:11.7318%">+2</td><td
      style="width:35.1955%">Advanced tricks, pirate trick</td></tr><tr><td
      style="width:9.78692%">7th</td><td style="width:20.1945%">+5</td><td
      style="width:11.9181%">+2</td><td style="width:10.8007%">+4</td><td
      style="width:11.7318%">+2</td><td style="width:35.1955%">Sneak attack
      +3d6</td></tr><tr><td style="width:9.78692%">8th</td><td
      style="width:20.1945%">+6</td><td style="width:11.9181%">+3</td><td
      style="width:10.8007%">+4</td><td style="width:11.7318%">+3</td><td
      style="width:35.1955%">Pirate trick</td></tr><tr><td
      style="width:9.78692%">9th</td><td style="width:20.1945%">+6</td><td
      style="width:11.9181%">+3</td><td style="width:10.8007%">+5</td><td
      style="width:11.7318%">+3</td><td style="width:35.1955%">Pirate
      trick</td></tr><tr><td style="width:9.78692%">10th</td><td
      style="width:20.1945%">+7</td><td style="width:11.9181%">+3</td><td
      style="width:10.8007%">+5</td><td style="width:11.7318%">+3</td><td
      style="width:35.1955%">Sneak attack +4d6</td></tr></tbody></table><p><br
      />The following are class features of the Inner Sea pirate prestige
      class.</p><p><strong>Weapon and Armor Proficiency</strong>: An Inner Sea
      pirate gains proficiency with the cutlass, hook hand, rapier, and short
      sword. An Inner Sea pirate gains no armor proficiency.</p><p><strong>Sneak
      Attack (Ex)</strong>: This ability is exactly like the rogue ability of
      the same name. The extra damage dealt increases by +1d6 at 4th level and
      every three levels thereafter. If an Inner Sea pirate gets a sneak attack
      bonus from another source, the damage bonuses stack.</p><p><strong>Pirate
      Talent</strong>: As an Inner Sea pirate develops her skill, she learns a
      number of tricks that aid her in her work. She gains a pirate trick at 4th
      level and every three levels thereafter. An Inner Sea pirate cannot select
      an individual trick more than once.</p><p>Tricks marked with an asterisk
      add effects to an Inner Sea pirate’s sneak attack. Only one of these
      tricks can be applied to an individual attack, and the decision must be
      made before the attack roll is made.</p><p><em>Classic Duelist (Ex)</em>:
      The Inner Sea pirate gains a +1 competence bonus on attack rolls made with
      a cutlass, rapier, or short sword.</p><p><em>Deep Breath (Ex)</em>: The
      Inner Sea pirate can hold her breath for a number of rounds equal to three
      times her Constitution modifier before she must start making Constitution
      checks.</p><p><em>Drink for Free (Ex)</em>: This ability presumes that the
      Inner Sea pirate is admired or feared in the taverns she frequents. The
      Inner Sea pirate designates one tavern per Inner Sea pirate level as a
      “favored tavern.” When in a favored tavern, the Inner Sea pirate drinks
      for free. In addition, she gains a +2 circumstance bonus on Diplomacy and
      Intimidate checks within a favored tavern.</p><p><em>Farseer (Ex)</em>:
      Pirates take turns serving as lookouts. An Inner Sea pirate with this
      ability reduces any distancebased penalties on Perception checks by half
      when she is at least 10 feet off the ground or above the deck of a
      ship.</p><p><em>Hand Stab* (Ex)</em>: The Inner Sea pirate can weaken an
      opponent’s weapon hand by hitting with a sneak attack. When she deals
      sneak attack damage against an opponent, that opponent becomes easier to
      disarm. Until the end of the rogue’s next turn, anyone attempting to
      disarm that opponent gains a +2 bonus on the disarm
      attempt.</p><p><em>Quick Appraise (Ex)</em>: The Inner Sea pirate must at
      times quickly select the most valuable items to spirit away. She appraises
      an item as a move action and can determine the most valuable item in a
      treasure hoard as a standard action.</p><p><em>Rigging Monkey (Ex)</em>:
      Some pirates become skilled at climbing on ropes and rigging. This ability
      grants the Inner Sea pirate a +2 bonus on Climb checks when using a rope
      to help her climb. In addition, when using a rope to help her climb, the
      Inner Sea pirate can move at half her speed. (By accepting a –5 penalty
      she can move at her normal speed.)<br /><br /><em>Storm Sailor (Ex)</em>:
      The Inner Sea pirate treats all storms as if they were one category less
      severe for the purposes of sailing and navigation. (This benefit stacks
      with the Hurricane Savvy character trait and other effects related to wind
      conditions.) She can make an Acrobatics check to move her normal speed
      across uneven ground, and she does not take a penalty on any Acrobatics
      checks due to slightly slippery, slightly unsteady, or moderately unsteady
      conditions.</p><p><em>Windrigger (Ex)</em>: The Inner Sea pirate has
      learned the tricky art of tacking. With a DC 15 Profession (sailor) check,
      she increases her ship’s base movement by 5 feet for 1
      hour.</p><p><strong>Advanced Tricks</strong>: At 6th, 8th, and 9th level,
      an Inner Sea pirate can choose one of the following advanced tricks in
      place of a pirate trick.</p><p><em>Burst of Speed (Ex)</em>: The Inner Sea
      pirate has learned to coax speed out of her ship when it really counts.
      With a DC 20 Profession (sailor) check, she doubles her ship’s base
      movement for 1 round. She can use this ability once per
      day.</p><p><em>Defensive Climber (Ex)</em>: The Inner Sea pirate does not
      lose her Dexterity bonus to AC when climbing.</p><p><em>Fearsome Advance*
      (Ex)</em>: If the Inner Sea pirate deals damage with her sneak attack, she
      can make an Intimidate check as an immediate action to demoralize the
      damaged opponent.</p><p><em>Foot Sweep* (Ex)</em>: The Inner Sea pirate
      has learned to sweep her opponents’ legs when they are reeling from the
      pain of her precise attacks. If the Inner Sea pirate deals damage with a
      sneak attack, she can make a trip attack against the damaged opponent as
      an immediate action. All the usual rules for trip attacks apply, and feats
      such as Improved Trip modify the attack normally.</p><p><em>Shipboard
      Authority (Ex)</em>: As a full-round action once per day, the Inner Sea
      pirate can shout orders to her crew. All allies within 30 feet of the
      Inner Sea pirate who can hear her gain a +1 morale bonus on attack rolls
      or on a particular skill check for 1 round per the Inner Sea pirate’s
      level. The Inner Sea pirate specifies which bonus applies when she uses
      this ability and may not change the bonus for the duration of this
      ability.</p>
  links:
    classAssociations:
      - level: 1
        name: Sneak Attack (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.bVJlqJpzLFfnA61b
      - level: 2
        name: Advanced Tricks
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.O93n7H9kMd1xC58Q
      - level: 6
        name: Pirate Talent
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.HRbY7bS8JXAi1w9b
  savingThrows:
    ref:
      value: high
  skillsPerLevel: 6
  sources:
    - id: PZO9422
      pages: '24'
  subType: prestige
  tag: innerSeaPirate
type: class

