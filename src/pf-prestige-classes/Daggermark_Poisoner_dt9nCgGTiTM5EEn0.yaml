_id: dt9nCgGTiTM5EEn0
_key: '!items!dt9nCgGTiTM5EEn0'
img: systems/pf1/icons/skills/weapon_43.jpg
name: Daggermark Poisoner
system:
  bab: med
  classSkills:
    blf: true
    dis: true
    esc: true
    hea: true
    ste: true
  description:
    value: >-
      <p>Poison is a common weapon of cultures and nations across Golarion, but
      the craft reaches its acme in the city of Daggermark, the festering heart
      of the chaotic and war-torn River Kingdoms. Members of the Daggermark
      Poisoners’ Guild and aspiring hopefuls experiment with their craft
      constantly, creating hundreds of potent toxins either to sell or use for
      their own nefarious purposes.</p><h2>Requirements</h2><p>To qualify to
      become a Daggermark poisoner, a character must fulfill all of the
      following criteria.</p><p><strong>Skills</strong>: Craft (alchemy) 5
      ranks, Craft (traps) 5 ranks, Heal 5 ranks, Sleight of Hand 5 ranks.<br
      /><strong>Special</strong>: Poison use class ability. The character must
      craft at least 1,000 gp worth of poisons and donate them to the Daggermark
      Poisoners’ Guild.</p><h2>Class Skills</h2><p>The Daggermark Poisoner's
      class skills are Bluff (Cha), Disguise (Cha), Escape Artist (Dex), Heal
      (Wis), Sleight of Hand (Dex), and Stealth (Dex).</p><p><strong>Skill
      Points at each Level</strong>: 4 + Int modifier.<br /><strong>Hit
      Die</strong>: d8.</p><h2>Class Features</h2><table class="inner"
      style="width:96.9314%"><tbody><tr><td
      style="width:9.80518%"><strong>Level</strong></td><td
      style="width:13.6585%"><strong>Base Attack Bonus</strong></td><td
      style="width:8.93855%"><strong>Fort Save</strong></td><td
      style="width:8.00745%"><strong>Ref Save</strong></td><td
      style="width:8.75233%"><strong>Will Save</strong></td><td
      style="width:50.4655%"><strong>Special</strong></td></tr><tr><td
      style="width:9.80518%">1st</td><td style="width:13.6585%">+0</td><td
      style="width:8.93855%">+1</td><td style="width:8.00745%">+0</td><td
      style="width:8.75233%">+0</td><td style="width:50.4655%">Master poisoner,
      poison resistance +2, quick poisoning</td></tr><tr><td
      style="width:9.80518%">2nd</td><td style="width:13.6585%">+1</td><td
      style="width:8.93855%">+1</td><td style="width:8.00745%">+1</td><td
      style="width:8.75233%">+1</td><td style="width:50.4655%">Toxic apothecary,
      toxic trick</td></tr><tr><td style="width:9.80518%">3rd</td><td
      style="width:13.6585%">+2</td><td style="width:8.93855%">+2</td><td
      style="width:8.00745%">+1</td><td style="width:8.75233%">+1</td><td
      style="width:50.4655%">Toxic manufactory, trapster +1</td></tr><tr><td
      style="width:9.80518%">4th</td><td style="width:13.6585%">+3</td><td
      style="width:8.93855%">+2</td><td style="width:8.00745%">+1</td><td
      style="width:8.75233%">+1</td><td style="width:50.4655%">Poison resistance
      +4, toxic trick</td></tr><tr><td style="width:9.80518%">5th</td><td
      style="width:13.6585%">+3</td><td style="width:8.93855%">+3</td><td
      style="width:8.00745%">+2</td><td style="width:8.75233%">+2</td><td
      style="width:50.4655%">Sneak attack +1d6, treacherous
      toxin</td></tr><tr><td style="width:9.80518%">6th</td><td
      style="width:13.6585%">+4</td><td style="width:8.93855%">+3</td><td
      style="width:8.00745%">+2</td><td style="width:8.75233%">+2</td><td
      style="width:50.4655%">Swift poisoning, toxic trick, trapster
      +2</td></tr><tr><td style="width:9.80518%">7th</td><td
      style="width:13.6585%">+5</td><td style="width:8.93855%">+4</td><td
      style="width:8.00745%">+2</td><td style="width:8.75233%">+2</td><td
      style="width:50.4655%">Poison resistance +6</td></tr><tr><td
      style="width:9.80518%">8th</td><td style="width:13.6585%">+6</td><td
      style="width:8.93855%">+4</td><td style="width:8.00745%">+3</td><td
      style="width:8.75233%">+3</td><td style="width:50.4655%">Instantaneous
      toxicology, toxic trick</td></tr><tr><td
      style="width:9.80518%">9th</td><td style="width:13.6585%">+6</td><td
      style="width:8.93855%">+5</td><td style="width:8.00745%">+3</td><td
      style="width:8.75233%">+3</td><td style="width:50.4655%">Poison immunity,
      trapster +3</td></tr><tr><td style="width:9.80518%">10th</td><td
      style="width:13.6585%">+7</td><td style="width:8.93855%">+5</td><td
      style="width:8.00745%">+3</td><td style="width:8.75233%">+3</td><td
      style="width:50.4655%">Sneak attack +2d6, toxic
      trick</td></tr></tbody></table><p><br />The following are class features
      of the Daggermark poisoner prestige class.</p><p><strong>Weapon and Armor
      Proficiency</strong>: A Daggermark poisoner gains no additional weapon or
      armor proficiencies.</p><p><strong>Master Poisoner (Ex)</strong>: A
      Daggermark poisoner adds 1/2 her level on Craft (alchemy) checks dealing
      with poison. In addition, she can use Craft (alchemy) to change the type
      of a poison with 1 hour of work and an alchemist’s lab. If the Craft
      (alchemy) check (DC equals the poison’s save DC) is successful, the
      poison’s type changes to contact, ingested, inhaled, or injury; on a
      failed check, the poison is ruined.</p><p><strong>Poison Resistance
      (Ex)</strong>: A Daggermark poisoner gains a +2 bonus on saves against
      poison, increasing to +4 at 4th level, +6 at 7th level, and total immunity
      at 9th level. Alchemist levels stack with Daggermark poisoner levels when
      determining the effect of this ability.</p><p><strong>Quick Poisoning
      (Ex)</strong>: A Daggermark poisoner can poison a weapon as a move action.
      She can create poisons with the Craft (alchemy) skill in half the normal
      amount of time.</p><p><strong>Toxic Apothecary (Ex and Sp)</strong>: At
      2nd level, a Daggermark poisoner can use <em>detect poison </em>at will
      (range 5 feet, caster level equal to the Daggermark poisoner’s class
      level) as a swift action. She gains a bonus equal to 1/2 her class level
      on Heal checks dealing with poison, and on a successful check she adds
      this bonus to the save bonus she provides her patient against the treated
      poison.</p><p><strong>Toxic Trick</strong>: At 2nd level and every two
      levels thereafter, a Daggermark poisoner learns a toxic trick. She can use
      a number of toxic tricks each day equal to her Intelligence modifier + 1/2
      her class level. The save DC of any saving throw called for by a toxic
      trick is equal to 10 + the Daggermark poisoner’s class level + her
      Intelligence modifier. Spell-like abilities have a caster level equal to
      her class level.</p><p><em>Choking Bomb (Su)</em>: As the ninja trick
      (<em>Ultimate Combat </em>15), but with a DC as noted
      above.</p><p><em>Combine Poison (Ex)</em>: A Daggermark poisoner can
      combine two different poisons without reducing their efficacy, applying
      them to the same weapon, object, or trap. A creature exposed to the
      poisons must save against both.</p><p><em>Concentrate Poison (Su)</em>: As
      the alchemist discovery (<em>Advanced Player’s Guide
      </em>29).</p><p><em>Launch Trap (Ex)</em>: As the ranger’s trapper
      archetype class feature (<em>Ultimate Magic </em>65).</p><p><em>Poison
      Bomb (Ex or Su)</em>: As the ninja trick (<em>Ultimate Combat</em> 15). A
      Daggermark poisoner must possess the smoke bomb toxic trick before
      selecting this trick.</p><p><em>Poison Conversion (Su)</em>: As the
      alchemist discovery (<em>Ultimate Combat </em>24).</p><p><em>Poison Trap
      (Ex or Su)</em>: As the ranger trap (<em>Ultimate Magic
      </em>64).</p><p><em>Slow-Acting Poison (Ex)</em>: When crafting a poison,
      a Daggermark poisoner may choose to create it with a delayed onset time.
      This delay must be at least 1 round but cannot be longer than 1 minute per
      class level.</p><p><em>Smoke Bomb (Ex or Su)</em>: As the ninja trick
      (<em>Ultimate Combat</em> 16), but no <em>ki </em>points are required.<br
      /><br /><em>Tailored Toxin (Ex)</em>: Choose one creature type (and
      subtype, for humanoids or outsiders). Poisons a Daggermark poisoner uses
      are particularly effective against creatures of this type, increasing the
      save DC by 2. This trick can be taken more than once; each time it applies
      to a different creature type (or subtype).</p><p><em>Toxic Magic
      (Sp)</em>: Choose one of the following spells or alchemical extracts as a
      spell-like ability: <em>accelerate poison</em><sup>APG</sup>, <em>delay
      poison</em>, <em>pernicious poison</em><sup>UM</sup>,<em> transmute potion
      to poison</em><sup>APG</sup>, or <em>venomous bolt</em><sup>APG</sup>.
      This trick can be taken more than once; each time it grants a different
      spell-like ability.</p><p><strong>Toxic Manufactory (Ex)</strong>: At 3rd
      level, when creating poisons or antitoxins a Daggermark poisoner can
      create a number of doses equal to her Intelligence modifier at one time
      (minimum 1). These additional doses do not increase the time required, but
      do increase the raw material cost accordingly. In addition, she uses the
      item’s gp value as its sp value when determining progress made with her
      Craft (alchemy) checks. If the Daggermark poisoner has the Master
      Alchemist<em>APG</em> feat, she may create a number of doses of poison or
      antitoxin at one time equal to twice her Intelligence modifier and uses
      the item’s pp value as its sp value when determining progress with her
      Craft (alchemy) checks.</p><p><strong>Trapster (Ex)</strong>: At 3rd
      level, a Daggermark poisoner adds her class level on Perception skill
      checks made to locate traps and on Craft and Disable Device checks made
      with poisonous traps. A Daggermark poisoner also adds a +1 bonus on attack
      rolls, save DCs, and Perception and Disable Device DCs for poisoned traps
      she creates. This bonus increases by 1 at 6th level and again at 9th
      level.</p>
  links:
    classAssociations:
      - level: 1
        name: Quick Poisoning (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.A0eC027HeUke4GT1
      - level: 1
        name: Trapster (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.K0modP8yUhhYONJh
      - level: 1
        name: Master Poisoner (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.LALfMIX6iQ7bGxlP
      - level: 2
        name: Toxic Apothecary (Ex and Sp)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.hLZST0nU9mM10Bga
      - level: 2
        name: Toxic Trick
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.BHRh4Kz6G9j5lvhN
      - level: 3
        name: Toxic Manufactory (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.WD8GoAUfMIzhdHSl
      - level: 5
        name: Sneak Attack (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.jNNOSzDKdnAqTrPm
      - level: 5
        name: Treacherous Toxin (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.907Z7ILJsnNnzYRg
      - level: 6
        name: Swift Poisoning (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.heldfSxz0K2rIdre
      - level: 8
        name: Instantaneous Toxicology (Ex)
        uuid: Compendium.pf1e-archetypes.pf-prestige-features.XXBEObHI6px12TAK
  savingThrows:
    fort:
      value: high
  skillsPerLevel: 4
  sources:
    - id: PZO9249
      pages: '18'
  subType: prestige
  tag: daggermarkPoisoner
type: class

