_id: i6ib1Bybr2PvdgQE
_key: '!items!i6ib1Bybr2PvdgQE'
img: systems/pf1/icons/items/inventory/horn-drinking.jpg
name: Raging Song (Boaster)
system:
  associations:
    classes:
      - Skald
  description:
    value: >-
      <p><strong>Level</strong>: 1</p><p>A skald is trained to use music,
      oration, and similar performances to inspire his allies to feats of
      strength and ferocity. At 1st level, a skald can use this ability for a
      number of rounds per day equal to 3 + his Charisma modifier. For each
      level thereafter, he can use raging song for 2 additional rounds per
      day.</p><p>Starting a raging song is a standard action, but it can be
      maintained each round as a free action. A raging song cannot be disrupted,
      but it ends immediately if the skald is killed, paralyzed, stunned,
      knocked unconscious, or otherwise prevented from taking a free action each
      round to maintain it. A raging song counts as the bard's bardic
      performance special ability for any effect that affects bardic
      performances. A skald may learn bard masterpieces (Pathfinder RPG Ultimate
      Magic 21).</p><p>A raging song has audible components, but not visual
      components. Affected allies must be able to hear the skald for the song to
      have any effect. A deaf skald has a 20% chance to fail when attempting to
      use a raging song. If he fails this check, the attempt still counts
      against his daily limit. Deaf creatures are immune to raging
      songs.</p><p>If a raging song affects allies, when the skald begins a
      raging song and at the start of each ally's turn in which they can hear
      the raging song, the skald's allies must decide whether to accept or
      refuse its effects. This is not an action. Unconscious allies
      automatically accept the song. If accepted, the raging song's effects last
      for that ally's turn or until the song ends, whichever comes
      first.</p><p>At 7th level, a skald can start a raging song as a move
      action instead of a standard action. At 13th level, a skald can start a
      raging song as a swift action instead.</p><p><i>Inspired Rage (Su): </i>At
      1st level, affected allies gain a +2 morale bonus to Strength and
      Constitution and a +1 morale bonus on Will saving throws, but also take a
      -1 penalty to AC. While under the effects of inspired rage, allies other
      than the skald cannot use any Charisma-, Dexterity-, or Intelligence-based
      skills (except Acrobatics, Fly, Intimidate, and Ride) or any ability that
      requires patience or concentration. At 4th level and every 4 levels
      thereafter, the song's bonuses on Will saves increase by 1; the penalty to
      AC doesn't change. At 8th and 16th levels, the song's bonuses to Strength
      and Constitution increase by 2. (Unlike the barbarian's rage ability,
      those affected are not fatigued after the song ends.)</p><p>If an ally has
      her own rage class ability (such as barbarian's rage, bloodrager's
      bloodrage, or skald's inspired rage), she may use the Strength,
      Constitution, and Will saving throw bonuses, as well as AC penalties,
      based on her own ability and level instead of those from the skald (still
      suffering no fatigue afterward). However, inspired rage does not allow the
      ally to activate abilities dependent on other rage class abilities, such
      as rage powers, blood casting, or bloodrager bloodlines; the ally must
      activate her own rage class ability in order to use these
      features.</p><p><i>Song of Endurance (Su): </i>At 3rd level, the boaster's
      raging song can inspire his allies to great feats of endurance. This
      raging song functions as song of marching except affected allies also gain
      the benefits of the Endurance feat in addition to the benefits of any of
      the following feats the boaster has (even if they don't meet the
      prerequisites): Deathless Initiate, Deathless Master, Deathless Zealot,
      Diehard, Fast Healer, Heroic Defiance, and Heroic Recovery.</p><p><i>Song
      of Marching (Su): </i>At 3rd level, a skald can use raging song to inspire
      his allies to move faster without suffering from fatigue. By expending 1
      round of raging song, the skald invigorates allies within 60 feet, who may
      hustle (Core Rulebook 170) for the next hour; this movement counts as a
      walk (not a hustle) for the purpose of accruing nonlethal damage and
      fatigue. The skald must continue to perform the song for the remainder of
      the hour, otherwise its effects end, but only 1 round of raging song is
      expended for that hour.</p><p><i>Song of Strength (Su): </i>At 6th level,
      a skald can use raging song to inspire his allies to superhuman feats of
      strength. Once each round while the skald uses this performance, allies
      within 60 feet who can hear the skald may add 1/2 the skald's level to a
      Strength check or Strength-based skill check.</p><p><i>Song of Surmounting
      (Su): </i>At 7th level, the boaster's raging song can inspire allies to
      move quickly and easily. By spending 1 round of raging song, the boaster
      can affect all allies within 90 feet for 1 hour. Affected allies gain a
      benefit according to the surrounding terrain (the benefits don't stack): a
      climb speed equal to half their base speed (forest, jungle, mountain,
      underground), a swim speed equal to their base speed (swamp, water), or a
      +10 foot enhancement bonus to their base speed (cold, desert, hill,
      plains, or urban). The skald must continue to perform the song for the
      remainder of the hour; otherwise, its effects end, but only 1 round of
      raging song is expended for that hour. At 13th level, the speed or bonus
      granted by this ability doubles. At 19th level, this ability grants triple
      the listed speed or bonus.</p><p><i>Dirge of Doom (Su): </i>At 10th level,
      a skald can create a sense of growing dread in his enemies, causing them
      to become shaken. This only affects enemies that are within 30 feet and
      able to hear the skald's performance. The effect persists for as long as
      the enemy is within 30 feet and the skald continues his performance. This
      cannot cause a creature to become frightened or panicked, even if the
      targets are already shaken from another effect. This is a sonic
      mind-affecting fear effect, and relies on audible
      components.</p><p><i>Frightful Boast (Su): </i>At 14th level, the
      boaster's raging song can frighten foes, as per the frightening tune
      bardic performance.</p>
  sources:
    - id: PZO1140
      pages: '84'
  subType: classFeat
  tags:
    - Boaster
type: feat

