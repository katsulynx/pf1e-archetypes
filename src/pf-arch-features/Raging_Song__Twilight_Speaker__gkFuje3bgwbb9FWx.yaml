_id: gkFuje3bgwbb9FWx
_key: '!items!gkFuje3bgwbb9FWx'
img: systems/pf1/icons/items/inventory/horn-drinking.jpg
name: Raging Song (Twilight Speaker)
system:
  associations:
    classes:
      - Skald
  description:
    value: >-
      <p><strong>Level</strong>: 1</p><p>A skald is trained to use music,
      oration, and similar performances to inspire his allies to feats of
      strength and ferocity. At 1st level, a skald can use this ability for a
      number of rounds per day equal to 3 + his Charisma modifier. For each
      level thereafter, he can use raging song for 2 additional rounds per
      day.</p><p>Starting a raging song is a standard action, but it can be
      maintained each round as a free action. A raging song cannot be disrupted,
      but it ends immediately if the skald is killed, paralyzed, stunned,
      knocked unconscious, or otherwise prevented from taking a free action each
      round to maintain it. A raging song counts as the bard's bardic
      performance special ability for any effect that affects bardic
      performances. A skald may learn bard masterpieces (Pathfinder RPG Ultimate
      Magic 21).</p><p>A raging song has audible components, but not visual
      components. Affected allies must be able to hear the skald for the song to
      have any effect. A deaf skald has a 20% chance to fail when attempting to
      use a raging song. If he fails this check, the attempt still counts
      against his daily limit. Deaf creatures are immune to raging
      songs.</p><p>If a raging song affects allies, when the skald begins a
      raging song and at the start of each ally's turn in which they can hear
      the raging song, the skald's allies must decide whether to accept or
      refuse its effects. This is not an action. Unconscious allies
      automatically accept the song. If accepted, the raging song's effects last
      for that ally's turn or until the song ends, whichever comes
      first.</p><p>At 7th level, a skald can start a raging song as a move
      action instead of a standard action. At 13th level, a skald can start a
      raging song as a swift action instead.</p><p><i>Inspired Devotion (Su):
      </i>The twilight speaker inspires fervor rather than fury. Affected allies
      gain a +1 competence bonus on attack rolls and a +1 morale bonus on saving
      throws. At 6th, 12th, and 18th levels, these bonuses increase by
      1.</p><p><i>Song of Marching (Su): </i>At 3rd level, a skald can use
      raging song to inspire his allies to move faster without suffering from
      fatigue. By expending 1 round of raging song, the skald invigorates allies
      within 60 feet, who may hustle (Core Rulebook 170) for the next hour; this
      movement counts as a walk (not a hustle) for the purpose of accruing
      nonlethal damage and fatigue. The skald must continue to perform the song
      for the remainder of the hour, otherwise its effects end, but only 1 round
      of raging song is expended for that hour.</p><p><i>Song of Understanding
      (Su): </i>At 6th level, a twilight speaker can use his raging song to
      create an aura that allows creatures to speak and understand each others'
      languages. By expending 4 rounds of raging song, the twilight speaker
      creates an aura that grants him and creatures in a 60-foot spread centered
      on him the effects of tongues. This aura uses the skald's level as the
      caster level for all purposes, including duration. A twilight speaker can
      dismiss this aura as a standard action.</p><p><i>Song of Secrecy (Su):
      </i>At 10th level, a twilight speaker can use his song to shroud allies
      from detection. Affected allies gain a bonus equal to half the twilight
      speaker's skald level on Stealth checks, and they can attempt such checks
      without cover or concealment. Song of secrecy is audible only to those
      affected.</p><p><i>Song of the Fallen (Su): </i>At 14th level, a skald can
      temporarily revive dead allies to continue fighting, with the same
      limitations as raise dead. The skald selects a dead ally within 60 feet
      and expends 1 round of raging song to bring that ally back to life. The
      revived ally is alive but staggered. Each round, the skald may expend
      another 1 round of raging song to keep that ally alive for another round.
      The ally automatically dies if the skald ends this performance or is
      interrupted. The skald may revive multiple allies with this ability
      (either at the same time or over successive rounds) but must expend 1
      round of raging song per revived ally per round to maintain the
      effect.</p>
  sources:
    - id: PZO9491
      pages: '12'
  subType: classFeat
  tags:
    - Twilight Speaker
type: feat

