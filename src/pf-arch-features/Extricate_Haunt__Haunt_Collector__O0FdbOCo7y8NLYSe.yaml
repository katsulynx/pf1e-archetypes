_id: O0FdbOCo7y8NLYSe
_key: '!items!O0FdbOCo7y8NLYSe'
img: icons/sundries/books/book-eye-purple.webp
name: Extricate Haunt (Haunt Collector)
system:
  associations:
    classes:
      - Occultist
  description:
    value: >-
      <p><strong>Level</strong>: 8</p><p>The haunt collector can temporarily
      exorcise his haunted implements' possessing entities to spontaneously
      create phenomena similar to haunts and imbue them with spells the
      occultist knows. As a full-round action that provokes attacks of
      opportunity, the haunt collector expends 1 point of mental focus to
      extricate an implement's spirit and infuse an adjacent square with its
      ghostly presence while granting it the power to deliver a spell from the
      implement's associated school (whose range is touch or greater) on the
      haunt collector's behalf. This action consumes the spell just as if it had
      been cast, but the energy is held in check by the created haunt until
      triggered by conditions set forth by the haunt collector. The conditions
      needed to trigger the haunt's spell effect must be clear, although they
      can be general, using the guidelines of the magic mouth spell. The haunt
      is stationary, and once the conditions for the trigger are met, the spell
      is discharged normally, though it now originates from the haunt's
      square.</p><p>The haunt itself is an invisible, incorporeal, spectral
      force, similar to a stationary unseen servant with an undead aura for the
      purposes of spells such as detect undead. It has a number of hit points
      equal to the double the level of the spell used to create it + the
      occultist's Intelligence modifier. If triggered during the same combat it
      is created, the haunt acts at initiative count 10; otherwise, when the
      trigger occurs, the haunt initiates combat and acts at initiative count 10
      on the surprise round. Other creatures must succeed at a Perception check
      (DC = 10 + the haunt collector's occultist level) to act in the surprise
      round. The haunt can be damaged by positive energy and anything else that
      can harm haunts, and if destroyed before it is triggered, it dissipates
      harmlessly. If the haunt is destroyed, it does not reform in its haunted
      implement until the occultist next invests his implements with mental
      focus. If the haunt doesn't trigger before the occultist next invests his
      implements with mental focus, the haunt dissolves at that point and
      reforms in its haunted implement.</p><p>The haunt collector can use this
      ability a number of times per day equal to 1 + his Intelligence modifier.
      While a haunting presence is extricated from its implement, the haunt
      collector does not receive the benefit of the implement's seance boon, nor
      can he call upon the implement's spirit bonus or use its focus powers,
      though he can still cast spells associated with the implement without
      penalty. If the spell is discharged successfully (rather than the haunt
      being destroyed), the spirit's presence returns to the haunt collector's
      implement the following round, and the seance boon and spirit bonus
      abilities are again available to the haunt collector.</p><p>At 12th level,
      the haunt collector can create a free-roaming haunt by expending an
      additional point of mental focus, granting it a fly speed of 10 feet with
      good maneuverability, which allows it to change locations or seek targets,
      under the restrictions for trigger conditions as outlined above. If the
      haunt wanders beyond medium range (measured from the haunt collector's
      current position), it is instantly destroyed.</p><p>At 16th level, the
      haunt collector can extricate an implement's spirit as a standard
      action.</p>
  sources:
    - id: PZO1135
      pages: '62'
  subType: classFeat
  tags:
    - Haunt Collector
type: feat

