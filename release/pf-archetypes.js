Hooks.once("ready", async function () {
    if(game.modules.get("pf1-statblock-converter").active) {
        Hooks.call("sbc.loadCustomCompendiums", [
            'pf1e-archetypes.pf-arch-features',
            'pf1e-archetypes.pf-archetypes',
            'pf1e-archetypes.pf-prestige-classes',
            'pf1e-archetypes.pf-prestige-features'
        ]);
    }
});